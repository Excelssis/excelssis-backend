package com.neoris.excelsis.model.entities;

//
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "conf_usu_country_inst")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class UserCountryInstitution implements Serializable {

	private static final long serialVersionUID = 6744447828455738372L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "id")
	private Long id;
	@JoinColumn(name = "id_user")
	@ManyToOne
	private User userId;
	@JoinColumn(name = "id_institution")
	@ManyToOne
	private Institucions institutionId;
	@Column(name = "create")
	@CreationTimestamp
	private Date create;
	@Column(name = "modify")
	@UpdateTimestamp
	private Date modify;
	@Column(name = "status")
	private boolean status = true;
	@Column(name = "principal")
	private boolean principal = true;
	@Column(name = "commentary")
	private String commentary;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}

	public Date getCreate() {
		return create;
	}

	public void setCreate(Date create) {
		this.create = create;
	}

	public Date getModify() {
		return modify;
	}

	public void setModify(Date modify) {
		this.modify = modify;
	}

	public User getUserId() {
		return userId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}
	public Institucions getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(Institucions institutionId) {
		this.institutionId = institutionId;
	}

	public boolean isPrincipal() {
		return principal;
	}

	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}
	
}
