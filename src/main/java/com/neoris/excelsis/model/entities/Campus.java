package com.neoris.excelsis.model.entities;

import java.io.Serializable;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "campus")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Campus implements Serializable{

	private static final long serialVersionUID = 6562760634244278154L;
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "native")
	@GenericGenerator(name="native",strategy ="native")
	@Column(name="id")
    private Long id;
	@Column(name="name")
    private String name;
	@Column(name="address")
    private String address;
	@Column(name="city")
    private String city;
	@Column(name="code")
    private String code;
	@Column(name="create_date")
	@CreationTimestamp
	private Date createDate;
	@Column(name="modify")
	@UpdateTimestamp
    private Date modify;
	@Column(name="status")
    private boolean status=true;
    @Column(name="commentary")
    private String commentary;
    @JoinColumn(name = "id_institution")
    @ManyToOne
	private Institucions institutionId;
    @JoinColumn(name = "id_installation")
    @ManyToOne
	private Installations installationId;
    
//    @ManyToMany(mappedBy="degrees_campus")
//	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
//    private List<Degrees> degrees = new ArrayList<Degrees>();
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModify() {
		return modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getCommentary() {
		return commentary;
	}
	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}
	public Institucions getInstitutionId() {
		return institutionId;
	}
	public void setInstitutionId(Institucions institutionId) {
		this.institutionId = institutionId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Installations getInstallationId() {
		return installationId;
	}
	public void setInstallationId(Installations installationId) {
		this.installationId = installationId;
	}
//	public List<Degrees> getDegrees() {
//		return degrees;
//	}
//	public void setDegrees(List<Degrees> degrees) {
//		this.degrees = degrees;
//	}
	
}
