package com.neoris.excelsis.model.entities;

//
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "access_components")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class AccessComponents implements Serializable {

	private static final long serialVersionUID = 6744447828455738372L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_access", referencedColumnName = "id", nullable = false)
	@JsonBackReference(value = "access")
	private Access accessId;

	@JoinColumn(name = "id_component")
	@ManyToOne
//	@JsonBackReference(value = "componentsId")
	private Component componentsId;

	@Column(name = "create_date")
	@CreationTimestamp
	private Date createDate;
	@Column(name = "modify")
	@UpdateTimestamp
	private Date modify;
	@Column(name = "status")
	private boolean status = true;
	@Column(name = "commentary")
	private String commentary;
	
	@Column(name = "function")
	private String function;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModify() {
		return modify;
	}

	public void setModify(Date modify) {
		this.modify = modify;
	}

//	public Integer getModuloId() {
//		return moduloId;
//	}
//	public void setModuloId(Integer moduloId) {
//		this.moduloId = moduloId;
//	}
//	public Integer getSubmoduloId() {
//		return submoduloId;
//	}
//	public void setSubmoduloId(Integer submoduloId) {
//		this.submoduloId = submoduloId;
//	}
	public Component getComponentsId() {
		return componentsId;
	}

	public void setComponentsId(Component componentId) {
		this.componentsId = componentId;
	}

//	public Integer getPaginaId() {
//		return paginaId;
//	}
//	public void setPaginaId(Integer paginaId) {
//		this.paginaId = paginaId;
//	}
	public Access getAccessId() {
		return accessId;
	}

	public void setAccessId(Access accessId) {
		this.accessId = accessId;
	}
	
	

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	@Override
	public String toString() {
		return "AccessComponents [id=" + id + ", accessId=" + accessId.getId() + ", componentId=" + componentsId.getId()
				+ ", createDate=" + createDate + ", modify=" + modify + ", status=" + status + ", commentary="
				+ commentary + "]";
	}

}
