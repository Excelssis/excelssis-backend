package com.neoris.excelsis.model.entities;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "strategic_guidelines")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class StrategicGuidelines implements Serializable{

	private static final long serialVersionUID = -1672815486532848732L;
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "native")
	@GenericGenerator(name="native",strategy ="native")
	@Column(name="id")
    private Long id;
	@Column(name="name")
    private String name;
	@Column(name="description")
    private String description;
	@Column(name="code")
    private String code;
	@Column(name="create_date")
	@CreationTimestamp
	private Date createDate;
	@Column(name="modify")
	@UpdateTimestamp
    private Date modify;
	@Column(name="status")
    private boolean status=true;
    @Column(name="commentary")
    private String commentary;
    @JoinColumn(name = "id_institutional_objects")
    @ManyToOne
	private InstitutionalObjects institutionObjectsId;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModify() {
		return modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getCommentary() {
		return commentary;
	}
	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public InstitutionalObjects getInstitutionObjectsId() {
		return institutionObjectsId;
	}
	public void setInstitutionObjectsId(InstitutionalObjects institutionObjectsId) {
		this.institutionObjectsId = institutionObjectsId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
}
