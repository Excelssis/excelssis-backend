package com.neoris.excelsis.model.entities;
//
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "users_roles")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UsersRoles implements Serializable{

	private static final long serialVersionUID = 6744447828455738372L;
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "native")
	@GenericGenerator(name="native",strategy ="native")
	@Column(name="id")
    private Long id;
	
	@ManyToOne
	@JoinColumn(name = "id_user", referencedColumnName = "id")
    @JsonBackReference(value="users")
    private User userId;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_rol")
//	@JsonBackReference(value="rol")
    private Role rolId;
	
	@Column(name="rol_principal")
    private boolean rolPrincipal;	
	@Column(name="create_date")
	@CreationTimestamp
	private Date createDate;
	@Column(name="modify")
	@UpdateTimestamp
    private Date modify;
	@Column(name="status")
    private boolean status;
    @Column(name="commentary")
    private String commentary;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public User getUsersId() {
		return userId;
	}
	public void setUsersId(User user) {
		this.userId = user;
	}
	public Role getRolId() {
		return rolId;
	}
	public void setRolId(Role rol) {
		this.rolId = rol;
	}
	public boolean isRolPrincipal() {
		return rolPrincipal;
	}
	public void setRolPrincipal(boolean rolPrincipal) {
		this.rolPrincipal = rolPrincipal;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getCommentary() {
		return commentary;
	}
	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}
	
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModify() {
		return modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}
	@Override
	public String toString() {
		return "UsersRoles [id=" + id + ", user=" + userId.getId()+ ", rol=" + rolId.getId()+ ", rolPrincipal=" + rolPrincipal
				+ ", status=" + status + ", commentary=" + commentary + "]";
	}
	public UsersRoles() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    
	
}
