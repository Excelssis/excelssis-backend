package com.neoris.excelsis.model.entities;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.mapping.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.neoris.excelsis.util.CustomListDeserializer;

@Entity
@Table(name = "users")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User implements Serializable{

	private static final long serialVersionUID = -2332547257273256681L;
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "native")
	@GenericGenerator(name="native",strategy ="native")
	@Column(name="id")
    private Long id;
	@Column(name="name")
    private String name;
	@Column(name="nickname")
    private String nickName;
	@Column(name="password")
    private String password;
	@Column(name="create_date")
	@CreationTimestamp
	private Date createDate;
	@Column(name="modify")
	@UpdateTimestamp
    private Date modify;
	@Column(name="status")
    private boolean status=true;
	@Column(name="date_init")
    private Date dateInit;
	@Column(name="date_fin")
    private Date dateFin;
	@Column(name="time_init")
	private int timeInit;
	@Column(name="time_fin")
	private int timeFin;
    @Column(name="commentary")
    private String commentary;
    @Column(name="default_icon")
    private Boolean defaultIcon;
    @Column(name="photo")
    private Blob photo;
    
    @OneToMany(targetEntity = UsersRoles.class, mappedBy="rolId")
    @JsonManagedReference(value="users")
	@JsonProperty("UserRoles")
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<UsersRoles> usersRoles = new ArrayList<UsersRoles>();
	
    public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModify() {
		return modify;
	}

	public void setModify(Date modify) {
		this.modify = modify;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getDateInit() {
		return dateInit;
	}

	public void setDateInit(Date dateInit) {
		this.dateInit = dateInit;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public int getTimeInit() {
		return timeInit;
	}

	public void setTimeInit(int timeInit) {
		this.timeInit = timeInit;
	}

	public int getTimeFin() {
		return timeFin;
	}

	public void setTimeFin(int timeFin) {
		this.timeFin = timeFin;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}
	
	public List<UsersRoles> getUsersRoles() {
		return usersRoles;
	}

	public void setUsersRoles(List<UsersRoles> usersRoles) {
		this.usersRoles = usersRoles;
	}
	
	

	public Boolean getDefaultIcon() {
		return defaultIcon;
	}

	public void setDefaultIcon(Boolean default_icon) {
		this.defaultIcon = default_icon;
	}

	public Blob getPhoto() {
		return photo;
	}

	public void setPhoto(Blob photo) {
		this.photo = photo;
	}
	

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", nickName=" + nickName + ", password=" + password
				+ ", createDate=" + createDate + ", modify=" + modify + ", status=" + status + ", dateInit=" + dateInit
				+ ", dateFin=" + dateFin + ", "
						+ "timeInit=" + timeInit + ", timeFin=" + timeFin + ", commentary="
				+ commentary +"]";
	}
	
	
}
