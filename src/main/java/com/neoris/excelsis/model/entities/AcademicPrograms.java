package com.neoris.excelsis.model.entities;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "academic_programs")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AcademicPrograms implements Serializable{

	private static final long serialVersionUID = 6562760634244278154L;
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "native")
	@GenericGenerator(name="native",strategy ="native")
	@Column(name="id")
    private Long id;
	@Column(name="name")
    private String name;
	@Column(name="description")
    private String description;
	@Column(name="code")
    private String code;
	
	@JoinColumn(name = "period_acad_ini")
    @ManyToOne
	private AcademicPeriods periodAcadIni;
	@JoinColumn(name = "period_acad_fin")
    @ManyToOne
	private AcademicPeriods periodAcadFin;
	@Column(name="date_ini")
    private Date dateIni;
	@Column(name="date_fin")
    private Date dateFin;
	@Column(name="create_date")
	@CreationTimestamp
	private Date createDate;
	@Column(name="modify")
	@UpdateTimestamp
    private Date modify;
	@Column(name="status")
    private boolean status=true;
    @Column(name="commentary")
    private String commentary;
    @JoinColumn(name = "id_institution")
    @ManyToOne
	private Institucions institutionId;
    @JoinColumn(name = "id_degree")
    @ManyToOne
	private Degrees degreeId;
    
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModify() {
		return modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getCommentary() {
		return commentary;
	}
	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}
	public Institucions getInstitutionId() {
		return institutionId;
	}
	public void setInstitutionId(Institucions institutionId) {
		this.institutionId = institutionId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Degrees getDegreeId() {
		return degreeId;
	}
	public void setDegreeId(Degrees degreeId) {
		this.degreeId = degreeId;
	}
	public Date getDateIni() {
		return dateIni;
	}
	public void setDateIni(Date dateIni) {
		this.dateIni = dateIni;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	public AcademicPeriods getPeriodAcadIni() {
		return periodAcadIni;
	}
	public void setPeriodAcadIni(AcademicPeriods periodAcadIni) {
		this.periodAcadIni = periodAcadIni;
	}
	public AcademicPeriods getPeriodAcadFin() {
		return periodAcadFin;
	}
	public void setPeriodAcadFin(AcademicPeriods periodAcadFin) {
		this.periodAcadFin = periodAcadFin;
	}
}
