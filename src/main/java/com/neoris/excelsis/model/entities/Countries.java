package com.neoris.excelsis.model.entities;
//
import java.io.Serializable;
import java.sql.Blob;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.mapping.Collection;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.neoris.excelsis.util.CustomListDeserializer;

@Entity
@Table(name = "countries")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Countries implements Serializable{

	private static final long serialVersionUID = -2332547257273256681L;
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "native")
	@GenericGenerator(name="native",strategy ="native")
	@Column(name="id")
    private Long id;
	@Column(name="name")
    private String name;
	@Column(name="description")
    private String description;
	@Column(name="formal_description")
    private String formalDescription;
	@Column(name="create_date")
	@CreationTimestamp
	private Date createDate;
	@Column(name="modify")
	@UpdateTimestamp
    private Date modify;
	@Column(name="status")
    private boolean status=true;
	@Column(name="code_dane")
    private String codeDane;
	@Column(name="code_prefijo")
    private String codePrefijo;
	@Column(name="code_postal")
    private String codePostal;
	@Column(name="code_country")
    private String codeCountry;
    @Column(name="commentary")
    private String commentary;
	
    public Countries() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFormalDescription() {
		return formalDescription;
	}

	public void setFormalDescription(String formalDescription) {
		this.formalDescription = formalDescription;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModify() {
		return modify;
	}

	public void setModify(Date modify) {
		this.modify = modify;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCodeDane() {
		return codeDane;
	}

	public void setCodeDane(String codeDane) {
		this.codeDane = codeDane;
	}

	public String getCodePrefijo() {
		return codePrefijo;
	}

	public void setCodePrefijo(String codePrefijo) {
		this.codePrefijo = codePrefijo;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getCodeCountry() {
		return codeCountry;
	}

	public void setCodeCountry(String codeCountry) {
		this.codeCountry = codeCountry;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}
	
	

//	public List<Institucions> getInstitutions() {
//		return institutions;
//	}
//
//	public void setInstitutions(List<Institucions> institutions) {
//		this.institutions = institutions;
//	}

	@Override
	public String toString() {
		return "Countries [id=" + id + ", name=" + name + ", description=" + description + ", formalDescription="
				+ formalDescription + ", createDate=" + createDate + ", modify=" + modify + ", status=" + status
				+ ", codeDane=" + codeDane + ", codePrefijo=" + codePrefijo + ", codePostal=" + codePostal
				+ ", codeCountry=" + codeCountry + ", commentary=" + commentary + "]";
	}
	
}
