package com.neoris.excelsis.model.entities;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "academic_periods")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AcademicPeriods implements Serializable{

	private static final long serialVersionUID = 6562760634244278154L;
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "native")
	@GenericGenerator(name="native",strategy ="native")
	@Column(name="id")
    private Long id;
	@Column(name="name")
    private String name;
	@Column(name="code")
    private String code;
	@Column(name="description")
    private String description;
	@Column(name="date_ini")
    private Date date_ini;
	@Column(name="date_fin")
    private Date date_fin;
	@Column(name="create_date")
	@CreationTimestamp
	private Date createDate;
	@Column(name="modify")
	@UpdateTimestamp
    private Date modify;
	@Column(name="status")
    private boolean status=true;
    @Column(name="commentary")
    private String commentary;
    @JoinColumn(name = "id_institution")
    @ManyToOne
	private Institucions institutionId;
    @JoinColumn(name = "id_degree")
    @ManyToOne
	private Degrees degreeId;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModify() {
		return modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getCommentary() {
		return commentary;
	}
	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}
	public Institucions getInstitutionId() {
		return institutionId;
	}
	public void setInstitutionId(Institucions institutionId) {
		this.institutionId = institutionId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Degrees getDegreeId() {
		return degreeId;
	}
	public void setDegreeId(Degrees degreeId) {
		this.degreeId = degreeId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate_ini() {
		return date_ini;
	}
	public void setDate_ini(Date date_ini) {
		this.date_ini = date_ini;
	}
	public Date getDate_fin() {
		return date_fin;
	}
	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}
}
