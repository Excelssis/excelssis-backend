package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.StrategicGuidelines;

public interface StrategicGuidelinesServicio {

		public void createStrategicGuidelines(StrategicGuidelines strategicGuidelines);
	    public List<StrategicGuidelines> getStrategicGuidelines();
	    public StrategicGuidelines findById(long id);
	    public StrategicGuidelines update(StrategicGuidelines strategicGuidelines, long l);
	    public void deleteStrategicGuidelinesById(long id);
	    public StrategicGuidelines updatePartially(StrategicGuidelines strategicGuidelines, long id);
	    public List<StrategicGuidelines> getAllStrategicGuidelines();
}
