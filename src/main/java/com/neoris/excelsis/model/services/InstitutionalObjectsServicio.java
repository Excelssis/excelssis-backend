package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.InstitutionalObjects;

public interface InstitutionalObjectsServicio {

		public void createInstitutionalObjects(InstitutionalObjects institutionalObjects);
	    public List<InstitutionalObjects> getInstitutionalObjects();
	    public InstitutionalObjects findById(long id);
	    public InstitutionalObjects update(InstitutionalObjects institutionalObjects, long l);
	    public void deleteInstitutionalObjectsById(long id);
	    public InstitutionalObjects updatePartially(InstitutionalObjects institutionalObjects, long id);
	    public List<InstitutionalObjects> getAllInstitutionalObjects();
}
