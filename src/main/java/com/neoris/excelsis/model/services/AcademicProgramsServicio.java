package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.AcademicPrograms;

public interface AcademicProgramsServicio {

		public void createAcademicPrograms(AcademicPrograms academicPrograms);
	    public List<AcademicPrograms> getAcademicPrograms();
	    public AcademicPrograms findById(long id);
	    public AcademicPrograms update(AcademicPrograms academicPrograms, long l);
	    public void deleteAcademicProgramsById(long id);
	    public AcademicPrograms updatePartially(AcademicPrograms academicPrograms, long id);
	    public List<AcademicPrograms> getAllAcademicPrograms();
}
