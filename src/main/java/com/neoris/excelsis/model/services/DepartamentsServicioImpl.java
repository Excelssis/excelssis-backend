package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.Departaments;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.repositories.DepartamentsRepositorio;

@Service
@Transactional
public class DepartamentsServicioImpl implements DepartamentsServicio{
	
	@Autowired
	DepartamentsRepositorio departamentsRepositorio;

	@Override
	public void createDepartaments(Departaments departaments) {
		// TODO Auto-generated method stub
		departamentsRepositorio.save(departaments);
	}

	@Override
	public List<Departaments> getDepartaments() {
		// TODO Auto-generated method stub
		return  (List<Departaments>) departamentsRepositorio.findAll();
	}

	@Override
	public Departaments findById(long id) {
		Departaments departaments = departamentsRepositorio.findById(id).get();
		return departaments;
	}

	@Override
	public Departaments update(Departaments departaments, long l) {
		departamentsRepositorio.save(departaments);
		return departaments;
	}

	@Override
	public void deleteDepartamentsById(long id) {
		departamentsRepositorio.deleteById(id);
	}

	@Override
	public Departaments updatePartially(Departaments departaments, long id) {
		departamentsRepositorio.save(departaments);
		return departaments;
	}


	@Override
	public List<Departaments> getAllDepartaments() {
		List<Departaments> departamentss = departamentsRepositorio.findAll();
		return departamentss;
	}
	
	
	

}
