package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;

public interface MisionServicio {

		public void createMision(Mision mision);
	    public List<Mision> getMision();
	    public Mision findById(long id);
	    public Mision update(Mision mision, long l);
	    public void deleteMisionById(long id);
	    public Mision updatePartially(Mision mision, long id);
	    public List<Mision> getAllMision();
}
