package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Component;

public interface ComponentServicio {

		public void createComponent(Component component);
	    public List<Component> getComponent();
	    public Component findById(long id);
	    public Component update(Component component, long l);
	    public void deleteComponentById(long id);
	    public Component updatePartially(Component component, long id);
}
