package com.neoris.excelsis.model.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.UserCountryInstitution;
import com.neoris.excelsis.model.repositories.UserCountryInstitutionRepositorio;

@Service
@Transactional
public class UserCountryInstitutionServicioImpl implements UserCountryInstitutionServicio{
    
    @Autowired
    UserCountryInstitutionRepositorio usuarioCiudadInstitucionRepositorio;

    @Override
    public void createUserCountryInstitution(UserCountryInstitution usuarioCiudadInstitucion) {
        // TODO Auto-generated method stub
    	usuarioCiudadInstitucionRepositorio.save(usuarioCiudadInstitucion);
    }

    @Override
    public List<UserCountryInstitution> getUserCountryInstitution() {
        // TODO Auto-generated method stub
        return  (List<UserCountryInstitution>) usuarioCiudadInstitucionRepositorio.findAll();
    }

    @Override
    public UserCountryInstitution findById(long id) {
        UserCountryInstitution usuarioCiudadInstitucion = usuarioCiudadInstitucionRepositorio.findById(id).get();
        return usuarioCiudadInstitucion;
    }

    @Override
    public UserCountryInstitution update(UserCountryInstitution usuarioCiudadInstitucion, long l) {
    	usuarioCiudadInstitucionRepositorio.save(usuarioCiudadInstitucion);
        return usuarioCiudadInstitucion;
    }

    @Override
    public void deleteUserCountryInstitutionById(long id) {
    	usuarioCiudadInstitucionRepositorio.deleteById(id);
    }

    @Override
    public UserCountryInstitution updatePartially(UserCountryInstitution usuarioCiudadInstitucion, long id) {
    	usuarioCiudadInstitucionRepositorio.save(usuarioCiudadInstitucion);
        return usuarioCiudadInstitucion;
    }
    
    public List<UserCountryInstitution> findByIdUser(long id) {
    	List<UserCountryInstitution> users = (List<UserCountryInstitution>) usuarioCiudadInstitucionRepositorio.findAll();
    	List<UserCountryInstitution> usersResp = new ArrayList<UserCountryInstitution>();
        for (UserCountryInstitution userCountryInstitution : users) {
			if(userCountryInstitution.getUserId().getId().equals(id)){
				usersResp.add(userCountryInstitution);
			}
		}
        return usersResp;
    }
}
