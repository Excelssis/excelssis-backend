package com.neoris.excelsis.model.services;

import java.util.List;
import com.neoris.excelsis.model.entities.AccessComponents;
import com.neoris.excelsis.model.entities.UserCountryInstitution;

public interface UserCountryInstitutionServicio {

		public void createUserCountryInstitution(UserCountryInstitution usuarioCiudadInstitucion);
	    public List<UserCountryInstitution> getUserCountryInstitution();
	    public UserCountryInstitution findById(long id);
	    public UserCountryInstitution update(UserCountryInstitution usuarioCiudadInstitucion, long l);
	    public void deleteUserCountryInstitutionById(long id);
	    public UserCountryInstitution updatePartially(UserCountryInstitution usuarioCiudadInstitucion, long id);
	    public List<UserCountryInstitution> findByIdUser(long id);
}