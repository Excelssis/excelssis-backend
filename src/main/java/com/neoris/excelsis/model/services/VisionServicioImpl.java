package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.Vision;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.repositories.VisionRepositorio;

@Service
@Transactional
public class VisionServicioImpl implements VisionServicio{
	
	@Autowired
	VisionRepositorio visionRepositorio;

	@Override
	public void createVision(Vision vision) {
		// TODO Auto-generated method stub
		visionRepositorio.save(vision);
	}

	@Override
	public List<Vision> getVision() {
		// TODO Auto-generated method stub
		return  (List<Vision>) visionRepositorio.findAll();
	}

	@Override
	public Vision findById(long id) {
		Vision vision = visionRepositorio.findById(id).get();
		return vision;
	}

	@Override
	public Vision update(Vision vision, long l) {
		visionRepositorio.save(vision);
		return vision;
	}

	@Override
	public void deleteVisionById(long id) {
		visionRepositorio.deleteById(id);
	}

	@Override
	public Vision updatePartially(Vision vision, long id) {
		visionRepositorio.save(vision);
		return vision;
	}


	@Override
	public List<Vision> getAllVision() {
		List<Vision> visions = visionRepositorio.findAll();
		return visions;
	}
	
	
	

}
