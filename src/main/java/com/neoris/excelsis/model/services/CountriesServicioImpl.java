package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.AccessComponents;
import com.neoris.excelsis.model.entities.Countries;
import com.neoris.excelsis.model.repositories.AccessComponentsRepositorio;
import com.neoris.excelsis.model.repositories.CountriesRepositorio;

@Service
@Transactional
public class CountriesServicioImpl implements CountriesServicio{
    
    @Autowired
    CountriesRepositorio countriesRepositorio;

    @Override
    public void createCountries(Countries accessComponents) {
        // TODO Auto-generated method stub
    	countriesRepositorio.save(accessComponents);
    }

    @Override
    public List<Countries> getCoutries() {
        // TODO Auto-generated method stub
        return  (List<Countries>) countriesRepositorio.findAll();
    }

    @Override
    public Countries findById(long id) {
    	Countries accessComponents = countriesRepositorio.findById(id).get();
        return accessComponents;
    }

    @Override
    public Countries update(Countries countries, long l) {
    	countriesRepositorio.save(countries);
        return countries;
    }

    @Override
    public void deleteCountriesById(long id) {
    	countriesRepositorio.deleteById(id);
    }

    @Override
    public Countries updatePartially(Countries countries, long id) {
    	countriesRepositorio.save(countries);
        return countries;
    }
}
