package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Access;
import com.neoris.excelsis.model.repositories.AccessRepositorio;

@Service
@Transactional
public class AccessServicioImpl implements AccessServicio{
	
	@Autowired
	AccessRepositorio accessRepositorio;

	@Override
	public void createAccess(Access access) {
		// TODO Auto-generated method stub
		accessRepositorio.save(access);
	}

	@Override
	public List<Access> getAccess() {
		// TODO Auto-generated method stub
		return  (List<Access>) accessRepositorio.findAll();
	}

	@Override
	public Access findById(long id) {
		Access access = accessRepositorio.findById(id).get();
		return access;
	}

	@Override
	public Access update(Access access, long l) {
		accessRepositorio.save(access);
		return access;
	}

	@Override
	public void deleteAccessById(long id) {
		accessRepositorio.deleteById(id);
	}

	@Override
	public Access updatePartially(Access access, long id) {
		accessRepositorio.save(access);
		return access;
	}
}
