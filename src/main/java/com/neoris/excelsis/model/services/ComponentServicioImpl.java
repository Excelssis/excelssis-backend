package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Component;
import com.neoris.excelsis.model.repositories.ComponentRepositorio;

@Service
@Transactional
public class ComponentServicioImpl implements ComponentServicio{
	
	@Autowired
	ComponentRepositorio componentRepositorio;

	@Override
	public void createComponent(Component component) {
		// TODO Auto-generated method stub
		componentRepositorio.save(component);
	}

	@Override
	public List<Component> getComponent() {
		// TODO Auto-generated method stub
		return  (List<Component>) componentRepositorio.findAll();
	}

	@Override
	public Component findById(long id) {
		Component component = componentRepositorio.findById(id).get();
		return component;
	}

	@Override
	public Component update(Component component, long l) {
		componentRepositorio.save(component);
		return component;
	}

	@Override
	public void deleteComponentById(long id) {
		componentRepositorio.deleteById(id);
	}

	@Override
	public Component updatePartially(Component component, long id) {
		componentRepositorio.save(component);
		return component;
	}
}
