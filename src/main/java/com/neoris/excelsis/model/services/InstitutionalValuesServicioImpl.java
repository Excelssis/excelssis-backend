package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.InstitutionalValues;
import com.neoris.excelsis.model.repositories.InstitutionalValuesRepositorio;

@Service
@Transactional
public class InstitutionalValuesServicioImpl implements InstitutionalValuesServicio{
	
	@Autowired
	InstitutionalValuesRepositorio institutionalValuesRepositorio;

	@Override
	public void createInstitutionalValues(InstitutionalValues institutionalValues) {
		// TODO Auto-generated method stub
		institutionalValuesRepositorio.save(institutionalValues);
	}

	@Override
	public List<InstitutionalValues> getInstitutionalValues() {
		// TODO Auto-generated method stub
		return  (List<InstitutionalValues>) institutionalValuesRepositorio.findAll();
	}

	@Override
	public InstitutionalValues findById(long id) {
		InstitutionalValues institutionalValues = institutionalValuesRepositorio.findById(id).get();
		return institutionalValues;
	}

	@Override
	public InstitutionalValues update(InstitutionalValues institutionalValues, long l) {
		institutionalValuesRepositorio.save(institutionalValues);
		return institutionalValues;
	}

	@Override
	public void deleteInstitutionalValuesById(long id) {
		institutionalValuesRepositorio.deleteById(id);
	}

	@Override
	public InstitutionalValues updatePartially(InstitutionalValues institutionalValues, long id) {
		institutionalValuesRepositorio.save(institutionalValues);
		return institutionalValues;
	}


	@Override
	public List<InstitutionalValues> getAllInstitutionalValues() {
		List<InstitutionalValues> institutionalValuess = institutionalValuesRepositorio.findAll();
		return institutionalValuess;
	}
	
	
	

}
