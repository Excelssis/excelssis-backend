package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.repositories.InstitucionsRepositorio;

@Service
@Transactional
public class InstitucionsServicioImpl implements InstitucionsServicio{
    
    @Autowired
    InstitucionsRepositorio institucionsRepositorio;

    @Override
    public void createInstitucions(Institucions institucions) {
        // TODO Auto-generated method stub
        institucionsRepositorio.save(institucions);
    }

    @Override
    public List<Institucions> getInstitucions() {
        // TODO Auto-generated method stub
        return  (List<Institucions>) institucionsRepositorio.findAll();
    }

    @Override
    public Institucions findById(long id) {
        Institucions institucions = institucionsRepositorio.findById(id).get();
        return institucions;
    }

    @Override
    public Institucions update(Institucions institucions, long l) {
        institucionsRepositorio.save(institucions);
        return institucions;
    }

    @Override
    public void deleteInstitucionsById(long id) {
        institucionsRepositorio.deleteById(id);
    }

    @Override
    public Institucions updatePartially(Institucions institucions, long id) {
        institucionsRepositorio.save(institucions);
        return institucions;
    }
}
