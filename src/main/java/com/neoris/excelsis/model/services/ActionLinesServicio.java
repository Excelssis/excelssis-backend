package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.ActionLines;

public interface ActionLinesServicio {

		public void createActionLines(ActionLines actionLines);
	    public List<ActionLines> getActionLines();
	    public ActionLines findById(long id);
	    public ActionLines update(ActionLines actionLines, long l);
	    public void deleteActionLinesById(long id);
	    public ActionLines updatePartially(ActionLines actionLines, long id);
	    public List<ActionLines> getAllActionLines();
}
