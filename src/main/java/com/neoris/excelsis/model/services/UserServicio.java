package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.User;

public interface UserServicio {

		public void createUser(User user);
	    public List<User> getUser();
	    public User findById(long id);
	    public User update(User user, long l);
	    public void deleteUserById(long id);
	    public User updatePartially(User user, long id);
	    public List<User> findByNickNamePass(String username, String password);
	    public List<User> getAllUser();
}
