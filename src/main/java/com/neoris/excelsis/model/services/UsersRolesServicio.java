package com.neoris.excelsis.model.services;

import java.util.Collection;
import java.util.List;

import com.neoris.excelsis.model.entities.Role;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.UsersRoles;

public interface UsersRolesServicio {

		public void createUsersRoles(UsersRoles usersRoles);
	    public List<UsersRoles> getUsersRoles();
	    public UsersRoles findById(long id);
	    public UsersRoles update(UsersRoles usersRoles, long l);
	    public void deleteUsersRolesById(long id);
	    public UsersRoles updatePartially(UsersRoles usersRoles, long id);
	    public Collection<UsersRoles> findByUserId(User userId);
}
