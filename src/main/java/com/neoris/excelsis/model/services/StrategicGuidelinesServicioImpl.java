package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.StrategicGuidelines;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.repositories.StrategicGuidelinesRepositorio;

@Service
@Transactional
public class StrategicGuidelinesServicioImpl implements StrategicGuidelinesServicio{
	
	@Autowired
	StrategicGuidelinesRepositorio strategicGuidelinesRepositorio;

	@Override
	public void createStrategicGuidelines(StrategicGuidelines strategicGuidelines) {
		// TODO Auto-generated method stub
		strategicGuidelinesRepositorio.save(strategicGuidelines);
	}

	@Override
	public List<StrategicGuidelines> getStrategicGuidelines() {
		// TODO Auto-generated method stub
		return  (List<StrategicGuidelines>) strategicGuidelinesRepositorio.findAll();
	}

	@Override
	public StrategicGuidelines findById(long id) {
		StrategicGuidelines strategicGuidelines = strategicGuidelinesRepositorio.findById(id).get();
		return strategicGuidelines;
	}

	@Override
	public StrategicGuidelines update(StrategicGuidelines strategicGuidelines, long l) {
		strategicGuidelinesRepositorio.save(strategicGuidelines);
		return strategicGuidelines;
	}

	@Override
	public void deleteStrategicGuidelinesById(long id) {
		strategicGuidelinesRepositorio.deleteById(id);
	}

	@Override
	public StrategicGuidelines updatePartially(StrategicGuidelines strategicGuidelines, long id) {
		strategicGuidelinesRepositorio.save(strategicGuidelines);
		return strategicGuidelines;
	}


	@Override
	public List<StrategicGuidelines> getAllStrategicGuidelines() {
		List<StrategicGuidelines> strategicGuideliness = strategicGuidelinesRepositorio.findAll();
		return strategicGuideliness;
	}
	
	
	

}
