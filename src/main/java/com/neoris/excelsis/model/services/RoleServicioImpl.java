package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Role;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.repositories.RoleRepositorio;
import com.neoris.excelsis.model.repositories.UserRepositorio;

@Service
@Transactional
public class RoleServicioImpl implements RoleServicio{
	
	@Autowired
	RoleRepositorio roleRepositorio;

	@Override
	public void createRole(Role role) {
		// TODO Auto-generated method stub
		roleRepositorio.save(role);
	}

	@Override
	public List<Role> getRole() {
		// TODO Auto-generated method stub
		return  (List<Role>) roleRepositorio.findAll();
	}

	@Override
	public Role findById(long id) {
		Role role = roleRepositorio.findById(id).get();
		return role;
	}

	@Override
	public Role update(Role role, long l) {
		roleRepositorio.save(role);
		return role;
	}

	@Override
	public void deleteRoleById(long id) {
		roleRepositorio.deleteById(id);
	}

	@Override
	public Role updatePartially(Role role, long id) {
		roleRepositorio.save(role);
		return role;
	}
	
	
	

}
