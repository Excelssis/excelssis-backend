package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.AcademicPrograms;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.repositories.AcademicProgramsRepositorio;

@Service
@Transactional
public class AcademicProgramsServicioImpl implements AcademicProgramsServicio{
	
	@Autowired
	AcademicProgramsRepositorio academicProgramsRepositorio;

	@Override
	public void createAcademicPrograms(AcademicPrograms academicPrograms) {
		// TODO Auto-generated method stub
		academicProgramsRepositorio.save(academicPrograms);
	}

	@Override
	public List<AcademicPrograms> getAcademicPrograms() {
		// TODO Auto-generated method stub
		return  (List<AcademicPrograms>) academicProgramsRepositorio.findAll();
	}

	@Override
	public AcademicPrograms findById(long id) {
		AcademicPrograms academicPrograms = academicProgramsRepositorio.findById(id).get();
		return academicPrograms;
	}

	@Override
	public AcademicPrograms update(AcademicPrograms academicPrograms, long l) {
		academicProgramsRepositorio.save(academicPrograms);
		return academicPrograms;
	}

	@Override
	public void deleteAcademicProgramsById(long id) {
		academicProgramsRepositorio.deleteById(id);
	}

	@Override
	public AcademicPrograms updatePartially(AcademicPrograms academicPrograms, long id) {
		academicProgramsRepositorio.save(academicPrograms);
		return academicPrograms;
	}


	@Override
	public List<AcademicPrograms> getAllAcademicPrograms() {
		List<AcademicPrograms> academicProgramss = academicProgramsRepositorio.findAll();
		return academicProgramss;
	}
	
	
	

}
