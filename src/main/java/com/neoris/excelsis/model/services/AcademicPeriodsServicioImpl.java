package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.AcademicPeriods;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.repositories.AcademicPeriodsRepositorio;

@Service
@Transactional
public class AcademicPeriodsServicioImpl implements AcademicPeriodsServicio{
	
	@Autowired
	AcademicPeriodsRepositorio academicPeriodsRepositorio;

	@Override
	public void createAcademicPeriods(AcademicPeriods academicPeriods) {
		// TODO Auto-generated method stub
		academicPeriodsRepositorio.save(academicPeriods);
	}

	@Override
	public List<AcademicPeriods> getAcademicPeriods() {
		// TODO Auto-generated method stub
		return  (List<AcademicPeriods>) academicPeriodsRepositorio.findAll();
	}

	@Override
	public AcademicPeriods findById(long id) {
		AcademicPeriods academicPeriods = academicPeriodsRepositorio.findById(id).get();
		return academicPeriods;
	}

	@Override
	public AcademicPeriods update(AcademicPeriods academicPeriods, long l) {
		academicPeriodsRepositorio.save(academicPeriods);
		return academicPeriods;
	}

	@Override
	public void deleteAcademicPeriodsById(long id) {
		academicPeriodsRepositorio.deleteById(id);
	}

	@Override
	public AcademicPeriods updatePartially(AcademicPeriods academicPeriods, long id) {
		academicPeriodsRepositorio.save(academicPeriods);
		return academicPeriods;
	}


	@Override
	public List<AcademicPeriods> getAllAcademicPeriods() {
		List<AcademicPeriods> academicPeriodss = academicPeriodsRepositorio.findAll();
		return academicPeriodss;
	}
	
	
	

}
