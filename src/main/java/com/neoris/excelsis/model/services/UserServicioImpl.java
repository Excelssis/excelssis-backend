package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.repositories.UserRepositorio;

@Service
@Transactional
public class UserServicioImpl implements UserServicio{
	
	@Autowired
	UserRepositorio userRepositorio;

	@Override
	public void createUser(User user) {
		// TODO Auto-generated method stub
		userRepositorio.save(user);
	}

	@Override
	public List<User> getUser() {
		// TODO Auto-generated method stub
		return  (List<User>) userRepositorio.findAll();
	}

	@Override
	public User findById(long id) {
		User user = userRepositorio.findById(id).get();
		return user;
	}

	@Override
	public User update(User user, long l) {
		userRepositorio.save(user);
		return user;
	}

	@Override
	public void deleteUserById(long id) {
		userRepositorio.deleteById(id);
	}

	@Override
	public User updatePartially(User user, long id) {
		userRepositorio.save(user);
		return user;
	}

	@Override
	public List<User> findByNickNamePass(String username, String password) {
		List<User> users = userRepositorio.findByNicknamePass(username, password);
		return users;
	}

	@Override
	public List<User> getAllUser() {
		List<User> users = userRepositorio.findAll();
		return users;
	}
	
	
	

}
