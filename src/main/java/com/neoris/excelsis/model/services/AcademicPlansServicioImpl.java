package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.AcademicPlans;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.repositories.AcademicPlansRepositorio;

@Service
@Transactional
public class AcademicPlansServicioImpl implements AcademicPlansServicio{
	
	@Autowired
	AcademicPlansRepositorio academicPlansRepositorio;

	@Override
	public void createAcademicPlans(AcademicPlans academicPlans) {
		// TODO Auto-generated method stub
		academicPlansRepositorio.save(academicPlans);
	}

	@Override
	public List<AcademicPlans> getAcademicPlans() {
		// TODO Auto-generated method stub
		return  (List<AcademicPlans>) academicPlansRepositorio.findAll();
	}

	@Override
	public AcademicPlans findById(long id) {
		AcademicPlans academicPlans = academicPlansRepositorio.findById(id).get();
		return academicPlans;
	}

	@Override
	public AcademicPlans update(AcademicPlans academicPlans, long l) {
		academicPlansRepositorio.save(academicPlans);
		return academicPlans;
	}

	@Override
	public void deleteAcademicPlansById(long id) {
		academicPlansRepositorio.deleteById(id);
	}

	@Override
	public AcademicPlans updatePartially(AcademicPlans academicPlans, long id) {
		academicPlansRepositorio.save(academicPlans);
		return academicPlans;
	}


	@Override
	public List<AcademicPlans> getAllAcademicPlans() {
		List<AcademicPlans> academicPlanss = academicPlansRepositorio.findAll();
		return academicPlanss;
	}
	
	
	

}
