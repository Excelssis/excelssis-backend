package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.AccessRoles;

public interface AccessRolesServicio {

		public void createAccessRoles(AccessRoles accessRoles);
	    public List<AccessRoles> getAccessRoles();
	    public AccessRoles findById(long id);
	    public AccessRoles update(AccessRoles accessRoles, long l);
	    public void deleteAccessRolesById(long id);
	    public AccessRoles updatePartially(AccessRoles accessRoles, long id);
}