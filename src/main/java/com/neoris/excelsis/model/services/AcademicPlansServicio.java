package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.AcademicPlans;

public interface AcademicPlansServicio {

		public void createAcademicPlans(AcademicPlans academicPlans);
	    public List<AcademicPlans> getAcademicPlans();
	    public AcademicPlans findById(long id);
	    public AcademicPlans update(AcademicPlans academicPlans, long l);
	    public void deleteAcademicPlansById(long id);
	    public AcademicPlans updatePartially(AcademicPlans academicPlans, long id);
	    public List<AcademicPlans> getAllAcademicPlans();
}
