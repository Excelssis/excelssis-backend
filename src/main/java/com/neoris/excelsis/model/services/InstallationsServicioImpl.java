package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.Installations;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.repositories.InstallationsRepositorio;

@Service
@Transactional
public class InstallationsServicioImpl implements InstallationsServicio{
	
	@Autowired
	InstallationsRepositorio installationsRepositorio;

	@Override
	public void createInstallations(Installations installations) {
		// TODO Auto-generated method stub
		installationsRepositorio.save(installations);
	}

	@Override
	public List<Installations> getInstallations() {
		// TODO Auto-generated method stub
		return  (List<Installations>) installationsRepositorio.findAll();
	}

	@Override
	public Installations findById(long id) {
		Installations installations = installationsRepositorio.findById(id).get();
		return installations;
	}

	@Override
	public Installations update(Installations installations, long l) {
		installationsRepositorio.save(installations);
		return installations;
	}

	@Override
	public void deleteInstallationsById(long id) {
		installationsRepositorio.deleteById(id);
	}

	@Override
	public Installations updatePartially(Installations installations, long id) {
		installationsRepositorio.save(installations);
		return installations;
	}


	@Override
	public List<Installations> getAllInstallations() {
		List<Installations> installationss = installationsRepositorio.findAll();
		return installationss;
	}
	
	
	

}
