package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.Degrees;

public interface DegreesServicio {

		public void createDegrees(Degrees degrees);
	    public List<Degrees> getDegrees();
	    public Degrees findById(long id);
	    public Degrees update(Degrees degrees, long l);
	    public void deleteDegreesById(long id);
	    public Degrees updatePartially(Degrees degrees, long id);
	    public List<Degrees> getAllDegrees();
}
