package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.Campus;

public interface CampusServicio {

		public void createCampus(Campus campus);
	    public List<Campus> getCampus();
	    public Campus findById(long id);
	    public Campus update(Campus campus, long l);
	    public void deleteCampusById(long id);
	    public Campus updatePartially(Campus campus, long id);
	    public List<Campus> getAllCampus();
}
