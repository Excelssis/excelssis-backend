package com.neoris.excelsis.model.services;

import java.util.List;
import com.neoris.excelsis.model.entities.AccessComponents;
import com.neoris.excelsis.model.entities.Institucions;

public interface InstitucionsServicio {

		public void createInstitucions(Institucions institucions);
	    public List<Institucions> getInstitucions();
	    public Institucions findById(long id);
	    public Institucions update(Institucions institucions, long l);
	    public void deleteInstitucionsById(long id);
	    public Institucions updatePartially(Institucions institucions, long id);
}