package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.Installations;

public interface InstallationsServicio {

		public void createInstallations(Installations installations);
	    public List<Installations> getInstallations();
	    public Installations findById(long id);
	    public Installations update(Installations installations, long l);
	    public void deleteInstallationsById(long id);
	    public Installations updatePartially(Installations installations, long id);
	    public List<Installations> getAllInstallations();
}
