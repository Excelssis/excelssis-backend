package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.InstitutionalObjects;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.repositories.InstitutionalObjectsRepositorio;

@Service
@Transactional
public class InstitutionalObjectsServicioImpl implements InstitutionalObjectsServicio{
	
	@Autowired
	InstitutionalObjectsRepositorio institutionalObjectsRepositorio;

	@Override
	public void createInstitutionalObjects(InstitutionalObjects institutionalObjects) {
		// TODO Auto-generated method stub
		institutionalObjectsRepositorio.save(institutionalObjects);
	}

	@Override
	public List<InstitutionalObjects> getInstitutionalObjects() {
		// TODO Auto-generated method stub
		return  (List<InstitutionalObjects>) institutionalObjectsRepositorio.findAll();
	}

	@Override
	public InstitutionalObjects findById(long id) {
		InstitutionalObjects institutionalObjects = institutionalObjectsRepositorio.findById(id).get();
		return institutionalObjects;
	}

	@Override
	public InstitutionalObjects update(InstitutionalObjects institutionalObjects, long l) {
		institutionalObjectsRepositorio.save(institutionalObjects);
		return institutionalObjects;
	}

	@Override
	public void deleteInstitutionalObjectsById(long id) {
		institutionalObjectsRepositorio.deleteById(id);
	}

	@Override
	public InstitutionalObjects updatePartially(InstitutionalObjects institutionalObjects, long id) {
		institutionalObjectsRepositorio.save(institutionalObjects);
		return institutionalObjects;
	}


	@Override
	public List<InstitutionalObjects> getAllInstitutionalObjects() {
		List<InstitutionalObjects> institutionalObjectss = institutionalObjectsRepositorio.findAll();
		return institutionalObjectss;
	}
	
	
	

}
