package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.repositories.MisionRepositorio;

@Service
@Transactional
public class MisionServicioImpl implements MisionServicio{
	
	@Autowired
	MisionRepositorio misionRepositorio;

	@Override
	public void createMision(Mision mision) {
		// TODO Auto-generated method stub
		misionRepositorio.save(mision);
	}

	@Override
	public List<Mision> getMision() {
		// TODO Auto-generated method stub
		return  (List<Mision>) misionRepositorio.findAll();
	}

	@Override
	public Mision findById(long id) {
		Mision mision = misionRepositorio.findById(id).get();
		return mision;
	}

	@Override
	public Mision update(Mision mision, long l) {
		misionRepositorio.save(mision);
		return mision;
	}

	@Override
	public void deleteMisionById(long id) {
		misionRepositorio.deleteById(id);
	}

	@Override
	public Mision updatePartially(Mision mision, long id) {
		misionRepositorio.save(mision);
		return mision;
	}


	@Override
	public List<Mision> getAllMision() {
		List<Mision> misions = misionRepositorio.findAll();
		return misions;
	}
	
	
	

}
