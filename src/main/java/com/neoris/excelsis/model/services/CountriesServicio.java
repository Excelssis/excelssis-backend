package com.neoris.excelsis.model.services;

import java.util.List;
import com.neoris.excelsis.model.entities.AccessComponents;
import com.neoris.excelsis.model.entities.Countries;

public interface CountriesServicio {

		public void createCountries(Countries coutries);
	    public List<Countries> getCoutries();
	    public Countries findById(long id);
	    public Countries update(Countries coutries, long l);
	    public void deleteCountriesById(long id);
	    public Countries updatePartially(Countries coutries, long id);
}