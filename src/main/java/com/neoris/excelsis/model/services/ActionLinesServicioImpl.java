package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.ActionLines;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.repositories.ActionLinesRepositorio;

@Service
@Transactional
public class ActionLinesServicioImpl implements ActionLinesServicio{
	
	@Autowired
	ActionLinesRepositorio actionLinesRepositorio;

	@Override
	public void createActionLines(ActionLines actionLines) {
		// TODO Auto-generated method stub
		actionLinesRepositorio.save(actionLines);
	}

	@Override
	public List<ActionLines> getActionLines() {
		// TODO Auto-generated method stub
		return  (List<ActionLines>) actionLinesRepositorio.findAll();
	}

	@Override
	public ActionLines findById(long id) {
		ActionLines actionLines = actionLinesRepositorio.findById(id).get();
		return actionLines;
	}

	@Override
	public ActionLines update(ActionLines actionLines, long l) {
		actionLinesRepositorio.save(actionLines);
		return actionLines;
	}

	@Override
	public void deleteActionLinesById(long id) {
		actionLinesRepositorio.deleteById(id);
	}

	@Override
	public ActionLines updatePartially(ActionLines actionLines, long id) {
		actionLinesRepositorio.save(actionLines);
		return actionLines;
	}


	@Override
	public List<ActionLines> getAllActionLines() {
		List<ActionLines> actionLiness = actionLinesRepositorio.findAll();
		return actionLiness;
	}
	
	
	

}
