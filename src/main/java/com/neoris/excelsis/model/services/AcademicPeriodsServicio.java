package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.AcademicPeriods;

public interface AcademicPeriodsServicio {

		public void createAcademicPeriods(AcademicPeriods academicPeriods);
	    public List<AcademicPeriods> getAcademicPeriods();
	    public AcademicPeriods findById(long id);
	    public AcademicPeriods update(AcademicPeriods academicPeriods, long l);
	    public void deleteAcademicPeriodsById(long id);
	    public AcademicPeriods updatePartially(AcademicPeriods academicPeriods, long id);
	    public List<AcademicPeriods> getAllAcademicPeriods();
}
