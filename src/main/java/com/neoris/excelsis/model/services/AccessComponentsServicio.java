package com.neoris.excelsis.model.services;

import java.util.List;
import com.neoris.excelsis.model.entities.AccessComponents;

public interface AccessComponentsServicio {

		public void createAccessComponents(AccessComponents accessComponents);
	    public List<AccessComponents> getAccessComponents();
	    public AccessComponents findById(long id);
	    public AccessComponents update(AccessComponents accessComponents, long l);
	    public void deleteAccessComponentsById(long id);
	    public AccessComponents updatePartially(AccessComponents accessComponents, long id);
}