package com.neoris.excelsis.model.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Role;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.UsersRoles;
import com.neoris.excelsis.model.repositories.RoleRepositorio;
import com.neoris.excelsis.model.repositories.UserRepositorio;
import com.neoris.excelsis.model.repositories.UsersRolesRepositorio;

@Service
@Transactional
public class UsersRolesServicioImpl implements UsersRolesServicio{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2471947068507962846L;
	@Autowired
	UsersRolesRepositorio usersRolesRepositorio;
	@Autowired
	RoleRepositorio roleRepositorio;

	@Override
	public void createUsersRoles(UsersRoles usersRoles) {
		// TODO Auto-generated method stub
		usersRolesRepositorio.save(usersRoles);
	}

	@Override
	public List<UsersRoles> getUsersRoles() {
		// TODO Auto-generated method stub
		return  (List<UsersRoles>) usersRolesRepositorio.findAll();
	}

	@Override
	public UsersRoles findById(long id) {
		UsersRoles usersRoles = usersRolesRepositorio.findById(id).get();
		return usersRoles;
	}

	@Override
	public UsersRoles update(UsersRoles usersRoles, long l) {
		usersRolesRepositorio.save(usersRoles);
		return usersRoles;
	}

	@Override
	public void deleteUsersRolesById(long id) {
		usersRolesRepositorio.deleteById(id);
	}

	@Override
	public UsersRoles updatePartially(UsersRoles usersRoles, long id) {
		usersRolesRepositorio.save(usersRoles);
		return usersRoles;
	}

	@Override
	public Collection<UsersRoles> findByUserId(User userId) {
		Collection<UsersRoles> usersRoles =	usersRolesRepositorio.findByUserId(userId);
		List<Role> roles = new ArrayList<>();
		for (UsersRoles ur : usersRoles) {
			Role role = roleRepositorio.findById(ur.getRolId().getId()).get();
			roles.add(role);
		}
		return usersRoles;
	}

	
	
	

}
