package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Access;

public interface AccessServicio {

		public void createAccess(Access access);
	    public List<Access> getAccess();
	    public Access findById(long id);
	    public Access update(Access access, long l);
	    public void deleteAccessById(long id);
	    public Access updatePartially(Access access, long id);
}
