package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.InstitutionalValues;

public interface InstitutionalValuesServicio {

		public void createInstitutionalValues(InstitutionalValues institutionalValues);
	    public List<InstitutionalValues> getInstitutionalValues();
	    public InstitutionalValues findById(long id);
	    public InstitutionalValues update(InstitutionalValues institutionalValues, long l);
	    public void deleteInstitutionalValuesById(long id);
	    public InstitutionalValues updatePartially(InstitutionalValues institutionalValues, long id);
	    public List<InstitutionalValues> getAllInstitutionalValues();
}
