package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.Degrees;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.repositories.DegreesRepositorio;

@Service
@Transactional
public class DegreesServicioImpl implements DegreesServicio{
	
	@Autowired
	DegreesRepositorio degreesRepositorio;

	@Override
	public void createDegrees(Degrees degrees) {
		// TODO Auto-generated method stub
		degreesRepositorio.save(degrees);
	}

	@Override
	public List<Degrees> getDegrees() {
		// TODO Auto-generated method stub
		return  (List<Degrees>) degreesRepositorio.findAll();
	}

	@Override
	public Degrees findById(long id) {
		Degrees degrees = degreesRepositorio.findById(id).get();
		return degrees;
	}

	@Override
	public Degrees update(Degrees degrees, long l) {
		degreesRepositorio.save(degrees);
		return degrees;
	}

	@Override
	public void deleteDegreesById(long id) {
		degreesRepositorio.deleteById(id);
	}

	@Override
	public Degrees updatePartially(Degrees degrees, long id) {
		degreesRepositorio.save(degrees);
		return degrees;
	}


	@Override
	public List<Degrees> getAllDegrees() {
		List<Degrees> degreess = degreesRepositorio.findAll();
		return degreess;
	}
	
	
	

}
