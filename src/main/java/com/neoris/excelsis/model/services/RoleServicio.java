package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Role;

public interface RoleServicio {

		public void createRole(Role role);
	    public List<Role> getRole();
	    public Role findById(long id);
	    public Role update(Role role, long l);
	    public void deleteRoleById(long id);
	    public Role updatePartially(Role role, long id);
}
