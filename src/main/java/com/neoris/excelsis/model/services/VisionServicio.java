package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.Vision;

public interface VisionServicio {

		public void createVision(Vision vision);
	    public List<Vision> getVision();
	    public Vision findById(long id);
	    public Vision update(Vision vision, long l);
	    public void deleteVisionById(long id);
	    public Vision updatePartially(Vision vision, long id);
	    public List<Vision> getAllVision();
}
