package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.AccessComponents;
import com.neoris.excelsis.model.repositories.AccessComponentsRepositorio;

@Service
@Transactional
public class AccessComponentsServicioImpl implements AccessComponentsServicio{
    
    @Autowired
    AccessComponentsRepositorio accessComponentsRepositorio;

    @Override
    public void createAccessComponents(AccessComponents accessComponents) {
        // TODO Auto-generated method stub
    	accessComponentsRepositorio.save(accessComponents);
    }

    @Override
    public List<AccessComponents> getAccessComponents() {
        // TODO Auto-generated method stub
        return  (List<AccessComponents>) accessComponentsRepositorio.findAll();
    }

    @Override
    public AccessComponents findById(long id) {
        AccessComponents accessComponents = accessComponentsRepositorio.findById(id).get();
        return accessComponents;
    }

    @Override
    public AccessComponents update(AccessComponents accessComponents, long l) {
    	accessComponentsRepositorio.save(accessComponents);
        return accessComponents;
    }

    @Override
    public void deleteAccessComponentsById(long id) {
    	accessComponentsRepositorio.deleteById(id);
    }

    @Override
    public AccessComponents updatePartially(AccessComponents accessComponents, long id) {
    	accessComponentsRepositorio.save(accessComponents);
        return accessComponents;
    }
}
