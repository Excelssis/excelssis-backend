package com.neoris.excelsis.model.services;

import java.util.List;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.Departaments;

public interface DepartamentsServicio {

		public void createDepartaments(Departaments departaments);
	    public List<Departaments> getDepartaments();
	    public Departaments findById(long id);
	    public Departaments update(Departaments departaments, long l);
	    public void deleteDepartamentsById(long id);
	    public Departaments updatePartially(Departaments departaments, long id);
	    public List<Departaments> getAllDepartaments();
}
