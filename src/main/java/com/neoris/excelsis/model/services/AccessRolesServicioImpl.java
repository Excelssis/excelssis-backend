package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.AccessRoles;
import com.neoris.excelsis.model.repositories.AccessRolesRepositorio;

@Service
@Transactional
public class AccessRolesServicioImpl implements AccessRolesServicio{
	
	@Autowired
	AccessRolesRepositorio accessRolesRepositorio;

	@Override
	public void createAccessRoles(AccessRoles accessRoles) {
		// TODO Auto-generated method stub
		accessRolesRepositorio.save(accessRoles);
	}

	@Override
	public List<AccessRoles> getAccessRoles() {
		// TODO Auto-generated method stub
		return  (List<AccessRoles>) accessRolesRepositorio.findAll();
	}

	@Override
	public AccessRoles findById(long id) {
		AccessRoles accessRoles = accessRolesRepositorio.findById(id).get();
		return accessRoles;
	}

	@Override
	public AccessRoles update(AccessRoles accessRoles, long l) {
		accessRolesRepositorio.save(accessRoles);
		return accessRoles;
	}

	@Override
	public void deleteAccessRolesById(long id) {
		accessRolesRepositorio.deleteById(id);
	}

	@Override
	public AccessRoles updatePartially(AccessRoles accessRoles, long id) {
		accessRolesRepositorio.save(accessRoles);
		return accessRoles;
	}
}
