package com.neoris.excelsis.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.Campus;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.repositories.CampusRepositorio;

@Service
@Transactional
public class CampusServicioImpl implements CampusServicio{
	
	@Autowired
	CampusRepositorio campusRepositorio;

	@Override
	public void createCampus(Campus campus) {
		// TODO Auto-generated method stub
		campusRepositorio.save(campus);
	}

	@Override
	public List<Campus> getCampus() {
		// TODO Auto-generated method stub
		return  (List<Campus>) campusRepositorio.findAll();
	}

	@Override
	public Campus findById(long id) {
		Campus campus = campusRepositorio.findById(id).get();
		return campus;
	}

	@Override
	public Campus update(Campus campus, long l) {
		campusRepositorio.save(campus);
		return campus;
	}

	@Override
	public void deleteCampusById(long id) {
		campusRepositorio.deleteById(id);
	}

	@Override
	public Campus updatePartially(Campus campus, long id) {
		campusRepositorio.save(campus);
		return campus;
	}


	@Override
	public List<Campus> getAllCampus() {
		List<Campus> campuss = campusRepositorio.findAll();
		return campuss;
	}
	
	
	

}
