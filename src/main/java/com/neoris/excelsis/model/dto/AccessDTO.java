package com.neoris.excelsis.model.dto;
//
import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.neoris.excelsis.model.entities.AccessComponents;
import com.neoris.excelsis.model.entities.Component;
import com.neoris.excelsis.util.CustomListDeserializer;


public class AccessDTO implements Serializable{

	private static final long serialVersionUID = -1738212964020407661L;
	
    private Long id;
    private String name;
    private String description;
	private Date createDate;
    private Date modify;
    private boolean status=true;
    private String commentary;
    
    @JsonManagedReference(value="access")
    @JsonProperty("AccessComponents")
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<AccessComponentsDTO> accessComponent = new ArrayList<AccessComponentsDTO>();
    
    
    public AccessDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModify() {
		return modify;
	}

	public void setModify(Date modify) {
		this.modify = modify;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public List<AccessComponentsDTO> getAccessComponent() {
		return accessComponent;
	}

	public void setAccessComponent(List<AccessComponentsDTO> accessComponent) {
		this.accessComponent = accessComponent;
	}

	@Override
	public String toString() {
		return "Access [id=" + id + ", name=" + name + ", description=" + description + ", createDate=" + createDate + ", modify="
				+ modify + ", status=" + status + ", commentary=" + commentary + "]";
	}	
}
