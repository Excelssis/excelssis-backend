package com.neoris.excelsis.model.dto;
//
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class AccessComponentsDTO implements Serializable{

	private static final long serialVersionUID = 6744447828455738372L;
	
    private Long id;
    @JsonBackReference(value="access")
	private AccessDTO accessId;
    
//    @JsonBackReference(value="componentsId")
    private ComponentDTO componentsId;
    
	private Date createDate;
	private Date modify;
	private boolean status=true;
    private String commentary;
    private String function;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getCommentary() {
		return commentary;
	}
	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}
	
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModify() {
		return modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}
	public AccessDTO getAccessId() {
		return accessId;
	}
	public void setAccessId(AccessDTO accessId) {
		this.accessId = accessId;
	}
	public ComponentDTO getComponentsId() {
		return componentsId;
	}
	public void setComponentsId(ComponentDTO componentId) {
		this.componentsId = componentId;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	
}
