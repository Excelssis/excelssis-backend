package com.neoris.excelsis.model.dto;
//
import java.io.Serializable;
import java.sql.Blob;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.mapping.Collection;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.neoris.excelsis.model.entities.UsersRoles;
import com.neoris.excelsis.util.CustomListDeserializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


//@Data
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor	
public class UserDTO implements Serializable{

	private static final long serialVersionUID = -2332547257273256681L;
    private Long id;
    private String name;
    private String nickName;
    private String password;
	private Date createDate;
    private Date modify;
    private boolean status=true;
    private Date dateInit;
    private Date dateFin;
	private int timeInit;
	private int timeFin;
    private String commentary;
    private Boolean defaultIcon;
    private Blob photo;
    private List<UsersRolesDTO> usersRoles = new ArrayList<UsersRolesDTO>();
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModify() {
		return modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public Date getDateInit() {
		return dateInit;
	}
	public void setDateInit(Date dateInit) {
		this.dateInit = dateInit;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	public int getTimeInit() {
		return timeInit;
	}
	public void setTimeInit(int timeInit) {
		this.timeInit = timeInit;
	}
	public int getTimeFin() {
		return timeFin;
	}
	public void setTimeFin(int timeFin) {
		this.timeFin = timeFin;
	}
	public String getCommentary() {
		return commentary;
	}
	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}
	public List<UsersRolesDTO> getUsersRoles() {
		return usersRoles;
	}
	public void setUsersRoles(List<UsersRolesDTO> usersRoles) {
		this.usersRoles = usersRoles;
	}
	public Boolean getDefaultIcon() {
		return defaultIcon;
	}
	public void setDefaultIcon(Boolean defaultIcon) {
		this.defaultIcon = defaultIcon;
	}
	public Blob getPhoto() {
		return photo;
	}
	public void setPhoto(Blob photo) {
		this.photo = photo;
	}
	
    
    
}
