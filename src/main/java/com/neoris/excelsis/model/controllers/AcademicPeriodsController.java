package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.AcademicPeriods;
import com.neoris.excelsis.model.entities.AcademicPrograms;
import com.neoris.excelsis.model.entities.Degrees;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.services.InstitucionsServicio;
import com.neoris.excelsis.model.services.MisionServicio;
import com.neoris.excelsis.model.services.AcademicPeriodsServicio;
import com.neoris.excelsis.model.services.AcademicProgramsServicio;
import com.neoris.excelsis.model.services.DegreesServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.output.LoginOutput;
import com.neoris.excelsis.security.GenericToken;

@RestController
@RequestMapping(value={"/excelsis-api/academicPeriods"})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.POST,RequestMethod.PUT})
public class AcademicPeriodsController {

	@Autowired
	AcademicPeriodsServicio academicPeriodsServicio;
	@Autowired
	InstitucionsServicio institucionsServicio;
	@Autowired
	DegreesServicio degreesServicio;
	@Autowired
	AcademicProgramsServicio academicProgramsServicio;
	
	@ResponseBody
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getacademicPeriodsById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        AcademicPeriods academicPeriods = academicPeriodsServicio.findById(id);
        GenericOutput academicPeriodsOutput = new GenericOutput();
        if (academicPeriods == null) {
        	academicPeriodsOutput.setMessages("ERROR");
        	academicPeriodsOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }else {
        	academicPeriodsOutput.setObject(academicPeriods);
        	academicPeriodsOutput.setToken(token);
        	academicPeriodsOutput.setMessages("OK");
        	academicPeriodsOutput.setCodError("200");
        }
        return new ResponseEntity<GenericOutput>(academicPeriodsOutput, HttpStatus.OK);
    }

	@ResponseBody
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createacademicPeriods(@RequestBody AcademicPeriods academicPeriods, @RequestHeader("Authorization") String token, UriComponentsBuilder ucBuilder){
		Institucions institucions=institucionsServicio.findById(academicPeriods.getInstitutionId().getId());
		Degrees degreess=degreesServicio.findById(academicPeriods.getDegreeId().getId());
		academicPeriods.setInstitutionId(institucions);
		academicPeriods.setDegreeId(degreess);
		academicPeriodsServicio.createAcademicPeriods(academicPeriods);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/academicPeriods/{id}").buildAndExpand(academicPeriods.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

	@ResponseBody
    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllAcademicPeriods(@RequestHeader("Authorization") String token) {
            ArrayList<AcademicPeriods> tasks=(ArrayList<AcademicPeriods>) academicPeriodsServicio.getAllAcademicPeriods();
            GenericOutput genericOutput = new GenericOutput();
            if(!tasks.isEmpty()){
            	genericOutput.setObject(tasks);
            	genericOutput.setToken(token);
        		genericOutput.setMessages("OK");
        		genericOutput.setCodError("200");
            }else{
            	genericOutput.setMessages("ERROR");
            	genericOutput.setCodError("400");
            }
            return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updateacademicPeriods(@RequestBody AcademicPeriods academicPeriods,@RequestHeader("Authorization") String token)
    {
        AcademicPeriods academicPeriodsUpdate = academicPeriodsServicio.findById(academicPeriods.getId());
        if (academicPeriods==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        
        Institucions institucions=institucionsServicio.findById(academicPeriods.getInstitutionId().getId());
		Degrees degreess=degreesServicio.findById(academicPeriods.getDegreeId().getId());
		academicPeriodsUpdate = academicPeriods;
		academicPeriodsUpdate.setInstitutionId(institucions);
		academicPeriodsUpdate.setDegreeId(degreess);
		academicPeriodsUpdate.setCreateDate(academicPeriodsUpdate.getCreateDate());
        academicPeriodsServicio.update(academicPeriodsUpdate, academicPeriodsUpdate.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericToken> deleteAcademicPeriods(@PathVariable("id") long id){
    	GenericOutput genericOutput = new GenericOutput();
    	AcademicPeriods academicPeriods = academicPeriodsServicio.findById(id);
        if (academicPeriods == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericToken>(HttpStatus.NOT_FOUND);
        }
        academicPeriodsServicio.deleteAcademicPeriodsById(id);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericToken>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/update/{id}", headers="Accept=application/json")
    public ResponseEntity<AcademicPeriods> updateAcademicPeriodsPartially(@PathVariable("id") long id, @RequestBody AcademicPeriods academicPeriods){
    	AcademicPeriods academicPeriodsUpdate = academicPeriodsServicio.findById(id);
        if(academicPeriods ==null){
            return new ResponseEntity<AcademicPeriods>(HttpStatus.NOT_FOUND);
        }
        academicPeriods.setCreateDate(academicPeriodsUpdate.getCreateDate());
        AcademicPeriods usr = academicPeriodsServicio.updatePartially(academicPeriods, id);
        return new ResponseEntity<AcademicPeriods>(academicPeriods, HttpStatus.OK);
    }
    
}
