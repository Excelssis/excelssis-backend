package com.neoris.excelsis.model.controllers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.AccessComponentsDTO;
import com.neoris.excelsis.model.dto.AccessDTO;
import com.neoris.excelsis.model.dto.UserDTO;
import com.neoris.excelsis.model.entities.Access;
import com.neoris.excelsis.model.entities.AccessComponents;
import com.neoris.excelsis.model.entities.Component;
import com.neoris.excelsis.model.entities.Countries;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.services.CountriesServicio;
import com.neoris.excelsis.output.GenericOutput;

@RestController
@RequestMapping(value={"/excelsis-api/countries"})
public class CountriesController {

	@Autowired
	CountriesServicio countriesServicio;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getcountriesById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        Countries countries = countriesServicio.findById(id);
        GenericOutput genericOutput = new GenericOutput();
        if (countries == null) {
        	genericOutput.setMessages("ERROR");
        	genericOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }
        genericOutput.setObject(countries);
        genericOutput.setMessages("OK");
		genericOutput.setCodError("200");
        return new ResponseEntity<GenericOutput>(genericOutput, HttpStatus.OK);
    }
	
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createcountries(@RequestHeader("Authorization") String token, @RequestBody Countries countries, UriComponentsBuilder ucBuilder){
    	HttpHeaders headers = new HttpHeaders();
    	countriesServicio.createCountries(countries);
        headers.setLocation(ucBuilder.path("/countries/{id}").buildAndExpand(countries.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value="/get", headers="Accept=application/json")
    public GenericOutput getAllCountries(@RequestHeader("Authorization") String token) {
    	GenericOutput genericOutput = new GenericOutput();
        List<Countries> tasks=countriesServicio.getCoutries();
        if(!tasks.isEmpty()){
        	genericOutput.setObject(tasks);
        	genericOutput.setToken(token);
    		genericOutput.setMessages("OK");
    		genericOutput.setCodError("200");
        }else{
        	genericOutput.setMessages("ERROR");
        	genericOutput.setCodError("400");
        }
        return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> updateCountries(@RequestHeader("Authorization") String token, @RequestBody Countries countries)
    {
    	try {
	        Countries countriesResp = countriesServicio.findById(countries.getId());
	        if (countries==null) {
	            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	        }
	        countriesResp.setCreateDate(countries.getCreateDate());
	        countriesServicio.update(countries, countries.getId());
    	}catch (Exception e) {
			// TODO: handle exception
    		System.out.println("ERROR!!!! "+e.getMessage());
		}
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericOutput> deleteCountries(@RequestHeader("Authorization") String token, @PathVariable("id") long id){
	    	Countries countries = countriesServicio.findById(id);
	    	GenericOutput genericOutput = new GenericOutput();
	        if (countries == null) {
	        	genericOutput.setMessages("ERROR");
	            genericOutput.setCodError("400");
	            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
	        }
	        countriesServicio.deleteCountriesById(id);
        return new ResponseEntity<GenericOutput>(HttpStatus.OK);
    }

    @PatchMapping(value="/{id}", headers="Accept=application/json")
    public ResponseEntity<Countries> updateCountriesPartially(@RequestHeader("Authorization") String token, @PathVariable("id") long id, @RequestBody Countries countries){
    	Countries countriesResp = countriesServicio.findById(id);
        if(countries ==null){
            return new ResponseEntity<Countries>(HttpStatus.NOT_FOUND);
        }
        countriesResp.setCreateDate(countries.getCreateDate());
        countriesServicio.updatePartially(countries, id);
        return new ResponseEntity<Countries>(countries, HttpStatus.OK);
    }
}
