package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.AccessComponentsDTO;
import com.neoris.excelsis.model.dto.AccessDTO;
import com.neoris.excelsis.model.dto.ComponentDTO;
import com.neoris.excelsis.model.entities.Access;
import com.neoris.excelsis.model.entities.AccessComponents;
import com.neoris.excelsis.model.entities.Component;
import com.neoris.excelsis.model.services.AccessComponentsServicio;
import com.neoris.excelsis.model.services.AccessServicio;
import com.neoris.excelsis.model.services.ComponentServicio;




@RestController
@RequestMapping(value={"/accessComponents"})
public class AccessComponentController {

	@Autowired
	AccessComponentsServicio accessComponentServicio;
	@Autowired
	ComponentServicio componentServicio; 
	@Autowired
	AccessServicio accessServicio;
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccessComponentsDTO> getcomponentsById(@PathVariable("id") long id) {
       AccessComponents components = accessComponentServicio.findById(id);
        if (components == null) {
            return new ResponseEntity<AccessComponentsDTO>(HttpStatus.NOT_FOUND);
        }
        AccessComponentsDTO componentDTO = modelMapper.map(components, AccessComponentsDTO.class);
        return new ResponseEntity<AccessComponentsDTO>(componentDTO, HttpStatus.OK);
    }

    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createAccessComponentss(@RequestBody AccessComponentsDTO componentDTO, UriComponentsBuilder ucBuilder){
    	AccessComponents components = modelMapper.map(componentDTO, AccessComponents.class);
    	accessComponentServicio.createAccessComponents(components);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/components/{id}").buildAndExpand(components.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<AccessComponentsDTO> getAllcomponents() {
            ArrayList<AccessComponents> tasks=(ArrayList<AccessComponents>) accessComponentServicio.getAccessComponents();
            System.out.println("AccessComponentController.getAllcomponents() TOTAL "+tasks.size());
            ArrayList<AccessComponentsDTO> componentDTOs = new ArrayList<AccessComponentsDTO>();
            for (AccessComponents component : tasks) {
            	System.out.println("AccessComponentController.getAllcomponents() "+component.toString());
            	System.out.println("AccessComponentController.getAllcomponents() COMPONENTE "+component.getComponentsId().getId());
            	System.out.println("AccessComponentController.getAllcomponents() ACCESS "+component.getAccessId().getId());
            	AccessComponentsDTO componentDTOPrincipal = modelMapper.map(component, AccessComponentsDTO.class);
            	
            	System.out.println("AccessComponentController.getAllcomponents() ACCESS DTO "+componentDTOPrincipal.getAccessId());
            	System.out.println("AccessComponentController.getAllcomponents() Comppnent DTO "+componentDTOPrincipal.getComponentsId());
            	if(component.getComponentsId().getId()!=0){
            		Component componentComponent = componentServicio.findById(component.getComponentsId().getId());
            		if(!componentComponent.getName().isEmpty()) {
                		ComponentDTO componentsDTOComponent = modelMapper.map(componentComponent, ComponentDTO.class);
                		componentDTOPrincipal.setComponentsId(componentsDTOComponent);
                	}
            	}
            	if(component.getAccessId().getId()!=0){
            		Access access = accessServicio.findById(component.getAccessId().getId());
            		if(!access.getName().isEmpty()) {
                		AccessDTO accessDTO = modelMapper.map(access, AccessDTO.class);
                		componentDTOPrincipal.setAccessId(accessDTO);
                	}
            	}
            	
            	System.out.println("AccessComponentController.getAllcomponents() ACCESS DTO 2 "+componentDTOPrincipal.getAccessId());
            	System.out.println("AccessComponentController.getAllcomponents() Comppnent DTO 2 "+componentDTOPrincipal.getComponentsId());
//            	if(componentDTOPrincipal.getModuloId().getId()!=0){
//            		Component componentModulo = componentServicio.findById(componentDTOPrincipal.getModuloId().getId());
//            		if(!componentModulo.getName().isEmpty()) {
//                		ComponentDTO componentsDTOModulo = modelMapper.map(componentModulo, ComponentDTO.class);
//                		componentDTOPrincipal.setModuloId(componentsDTOModulo);
//                	}
//            	}
//            	if(componentDTOPrincipal.getSubmoduloId().getId()!=0){
//            		Component componentSubModulo = componentServicio.findById(componentDTOPrincipal.getSubmoduloId().getId());
//            		if(!componentSubModulo.getName().isEmpty()) {
//                		ComponentDTO componentsDTOSubModulo = modelMapper.map(componentSubModulo, ComponentDTO.class);
//                		componentDTOPrincipal.setSubmoduloId(componentsDTOSubModulo);
//                	}
//            	}
            	
//            	if(componentDTOPrincipal.getPaginaId().getId()!=0){
//            		Component componentPagina = componentServicio.findById(componentDTOPrincipal.getPaginaId().getId());
//            		if(!componentPagina.getName().isEmpty()) {
//                		ComponentDTO componentsDTOModulo = modelMapper.map(componentPagina, ComponentDTO.class);
//                		componentDTOPrincipal.setPaginaId(componentsDTOModulo);
//                	}
//            	}
            	componentDTOs.add(componentDTOPrincipal);
			}
            return componentDTOs;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updatecomponents(@RequestBody AccessComponentsDTO componentDTO)
    {
        AccessComponents components = accessComponentServicio.findById(componentDTO.getId());
        if (components==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        componentDTO.setCreateDate(components.getCreateDate());
        componentDTO = modelMapper.map(components, AccessComponentsDTO.class);
        accessComponentServicio.update(components, components.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/{id}", headers ="Accept=application/json")
    public ResponseEntity<AccessComponentsDTO> deleteAccessComponentss(@PathVariable("id") long id){
    	AccessComponents components = accessComponentServicio.findById(id);
        if (components == null) {
            return new ResponseEntity<AccessComponentsDTO>(HttpStatus.NOT_FOUND);
        }
        accessComponentServicio.deleteAccessComponentsById(id);
        return new ResponseEntity<AccessComponentsDTO>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/{id}", headers="Accept=application/json")
    public ResponseEntity<AccessComponentsDTO> updateAccessComponentssPartially(@PathVariable("id") long id, @RequestBody AccessComponentsDTO componentDTO){
    	AccessComponents components = accessComponentServicio.findById(id);
        if(components ==null){
            return new ResponseEntity<AccessComponentsDTO>(HttpStatus.NOT_FOUND);
        }
        componentDTO.setCreateDate(components.getCreateDate());
        components = modelMapper.map(componentDTO, AccessComponents.class);
        accessComponentServicio.updatePartially(components, id);
        return new ResponseEntity<AccessComponentsDTO>(componentDTO, HttpStatus.OK);
    }
}
