package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.AcademicPeriods;
import com.neoris.excelsis.model.entities.AcademicPrograms;
import com.neoris.excelsis.model.entities.Degrees;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.services.InstitucionsServicio;
import com.neoris.excelsis.model.services.MisionServicio;
import com.neoris.excelsis.model.services.AcademicPeriodsServicio;
import com.neoris.excelsis.model.services.AcademicProgramsServicio;
import com.neoris.excelsis.model.services.DegreesServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.output.LoginOutput;
import com.neoris.excelsis.security.GenericToken;

@RestController
@RequestMapping(value={"/excelsis-api/academicPrograms"})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.POST,RequestMethod.PUT})
public class AcademicProgramsController {

	@Autowired
	AcademicProgramsServicio academicProgramsServicio;
	@Autowired
	InstitucionsServicio institucionsServicio;
	@Autowired
	DegreesServicio degreesServicio;
	@Autowired
	AcademicPeriodsServicio academicPeriodsServicio;
	
	
	@ResponseBody
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getacademicProgramsById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        AcademicPrograms academicPrograms = academicProgramsServicio.findById(id);
        GenericOutput academicProgramsOutput = new GenericOutput();
        if (academicPrograms == null) {
        	academicProgramsOutput.setMessages("ERROR");
        	academicProgramsOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }else {
        	academicProgramsOutput.setObject(academicPrograms);
        	academicProgramsOutput.setToken(token);
        	academicProgramsOutput.setMessages("OK");
        	academicProgramsOutput.setCodError("200");
        }
        return new ResponseEntity<GenericOutput>(academicProgramsOutput, HttpStatus.OK);
    }

	@ResponseBody
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createacademicPrograms(@RequestBody AcademicPrograms academicPrograms, @RequestHeader("Authorization") String token, UriComponentsBuilder ucBuilder){
		Institucions institucions=institucionsServicio.findById(academicPrograms.getInstitutionId().getId());
		academicPrograms.setInstitutionId(institucions);
		Degrees degreess=degreesServicio.findById(academicPrograms.getDegreeId().getId());
		AcademicPeriods academicPeriodsInit = academicPeriodsServicio.findById(academicPrograms.getPeriodAcadIni().getId());
		AcademicPeriods academicPeriodsFin = academicPeriodsServicio.findById(academicPrograms.getPeriodAcadFin().getId());
		academicPrograms.setInstitutionId(institucions);
		academicPrograms.setDegreeId(degreess);
		academicPrograms.setPeriodAcadIni(academicPeriodsInit);
		academicPrograms.setPeriodAcadFin(academicPeriodsFin);
		academicProgramsServicio.createAcademicPrograms(academicPrograms);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/academicPrograms/{id}").buildAndExpand(academicPrograms.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

	@ResponseBody
    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllAcademicPrograms(@RequestHeader("Authorization") String token) {
            ArrayList<AcademicPrograms> tasks=(ArrayList<AcademicPrograms>) academicProgramsServicio.getAllAcademicPrograms();
            GenericOutput genericOutput = new GenericOutput();
            if(!tasks.isEmpty()){
            	genericOutput.setObject(tasks);
            	genericOutput.setToken(token);
        		genericOutput.setMessages("OK");
        		genericOutput.setCodError("200");
            }else{
            	genericOutput.setMessages("ERROR");
            	genericOutput.setCodError("400");
            }
            return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updateacademicPrograms(@RequestBody AcademicPrograms academicPrograms,@RequestHeader("Authorization") String token)
    {
        AcademicPrograms academicProgramsUpdate = academicProgramsServicio.findById(academicPrograms.getId());
        if (academicPrograms==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        
        Institucions institucions=institucionsServicio.findById(academicPrograms.getInstitutionId().getId());
        academicProgramsUpdate.setInstitutionId(institucions);
		
		academicProgramsUpdate = academicPrograms;
		academicProgramsUpdate.setCreateDate(academicProgramsUpdate.getCreateDate());
        academicProgramsServicio.update(academicProgramsUpdate, academicProgramsUpdate.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericToken> deleteAcademicPrograms(@PathVariable("id") long id){
    	GenericOutput genericOutput = new GenericOutput();
    	AcademicPrograms academicPrograms = academicProgramsServicio.findById(id);
        if (academicPrograms == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericToken>(HttpStatus.NOT_FOUND);
        }
        academicProgramsServicio.deleteAcademicProgramsById(id);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericToken>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/update/{id}", headers="Accept=application/json")
    public ResponseEntity<AcademicPrograms> updateAcademicProgramsPartially(@PathVariable("id") long id, @RequestBody AcademicPrograms academicPrograms){
    	AcademicPrograms academicProgramsUpdate = academicProgramsServicio.findById(id);
        if(academicPrograms ==null){
            return new ResponseEntity<AcademicPrograms>(HttpStatus.NOT_FOUND);
        }
        academicPrograms.setCreateDate(academicProgramsUpdate.getCreateDate());
        AcademicPrograms usr = academicProgramsServicio.updatePartially(academicPrograms, id);
        return new ResponseEntity<AcademicPrograms>(academicPrograms, HttpStatus.OK);
    }
    
}
