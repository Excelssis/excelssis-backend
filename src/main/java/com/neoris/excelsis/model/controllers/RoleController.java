package com.neoris.excelsis.model.controllers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.AccessComponentsDTO;
import com.neoris.excelsis.model.dto.AccessDTO;
import com.neoris.excelsis.model.dto.AccessRolesDTO;
import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.dto.UserDTO;
import com.neoris.excelsis.model.entities.Access;
import com.neoris.excelsis.model.entities.AccessComponents;
import com.neoris.excelsis.model.entities.AccessRoles;
import com.neoris.excelsis.model.entities.Component;
import com.neoris.excelsis.model.entities.Role;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.services.AccessRolesServicio;
import com.neoris.excelsis.model.services.RoleServicio;
import com.neoris.excelsis.output.GenericOutput;

@RestController
@RequestMapping(value={"/excelsis-api/role"})
public class RoleController {

	@Autowired
	RoleServicio roleServicio;
	@Autowired
	AccessRolesServicio accessRolesServicio;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getroleById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        Role role = roleServicio.findById(id);
        GenericOutput genericOutput = new GenericOutput();
        if (role == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }
//        RoleDTO roleDTO = new RoleDTO();
//        roleDTO = modelMapper.map(role, RoleDTO.class);
//        roleDTO.setAccessRoles(getAccessRoles(role.getAccessRoles()));
        genericOutput.setObject(role);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericOutput>(genericOutput, HttpStatus.OK);
    }
	
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createrole(@RequestHeader("Authorization") String token, @RequestBody RoleDTO roledto, UriComponentsBuilder ucBuilder){
    	HttpHeaders headers = new HttpHeaders();
    	Role role = new Role();
    	role.setName(roledto.getName());
    	role.setDescription(roledto.getDescription());
    	role.setStatus(true);
    	roleServicio.createRole(role);
    	
    	for (AccessRolesDTO index : roledto.getAccessRoles()) {
    		  Access access = modelMapper.map(index.getAccessId(), Access.class);
        	  AccessRoles accessRoles = new AccessRoles();
        	  accessRoles.setAccessId(access);
        	  accessRoles.setRolId(role);
        	  accessRolesServicio.createAccessRoles(accessRoles);
          }
    	
        headers.setLocation(ucBuilder.path("/role/{id}").buildAndExpand(role.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value="/get", headers="Accept=application/json")
    public GenericOutput getAllRole(@RequestHeader("Authorization") String token) {
    	GenericOutput genericOutput = new GenericOutput();
        List<Role> tasks=roleServicio.getRole();
        List<RoleDTO> roleDTOs = new ArrayList<RoleDTO>();
        if(!tasks.isEmpty()) {
//        	for (Role role : tasks) {
//            	RoleDTO roleDTO = modelMapper.map(role, RoleDTO.class);
//            	roleDTO.setAccessRoles(getAccessRoles(role.getAccessRoles()));
//            	roleDTOs.add(roleDTO);
//    		}
        	genericOutput.setObject(tasks);
            genericOutput.setMessages("OK");
            genericOutput.setCodError("200");
        }else {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
        }
        
        return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> updateRole(@RequestHeader("Authorization") String token, @RequestBody RoleDTO roleDTO)
    {
    	try {
	        Role role = roleServicio.findById(roleDTO.getId());
	        if (role==null) {
	            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	        }
	        roleDTO.setCreateDate(role.getCreateDate());
	        role = modelMapper.map(roleDTO, Role.class);
	        roleServicio.update(role, role.getId());
    	}catch (Exception e) {
			// TODO: handle exception
    		System.out.println("ERROR!!!! "+e.getMessage());
		}
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<RoleDTO> deleteRole(@RequestHeader("Authorization") String token, @PathVariable("id") long id){
	    	Role role = roleServicio.findById(id);
	        if (role == null) {
	            return new ResponseEntity<RoleDTO>(HttpStatus.NOT_FOUND);
	        }
	        RoleDTO roleDTO = modelMapper.map(role, RoleDTO.class);
	        roleServicio.deleteRoleById(id);
        return new ResponseEntity<RoleDTO>(HttpStatus.OK);
    }

    @PatchMapping(value="/{id}", headers="Accept=application/json")
    public ResponseEntity<RoleDTO> updateRolePartially(@RequestHeader("Authorization") String token, @PathVariable("id") long id, @RequestBody RoleDTO roleDTO){
    	Role role = roleServicio.findById(id);
        if(role ==null){
            return new ResponseEntity<RoleDTO>(HttpStatus.NOT_FOUND);
        }
        roleDTO.setCreateDate(role.getCreateDate());
        role = modelMapper.map(roleDTO, Role.class);
        roleServicio.updatePartially(role, id);
        return new ResponseEntity<RoleDTO>(roleDTO, HttpStatus.OK);
    }
    
    public List<AccessRolesDTO> getAccessRoles(List<AccessRoles> accessRoles) {
    	List<AccessRolesDTO> resp = new ArrayList<AccessRolesDTO>();
    	for (AccessRoles index : accessRoles) {
			AccessRolesDTO accessRolesDTO = modelMapper.map(index, AccessRolesDTO.class);
			AccessDTO accessDTO = modelMapper.map(index.getAccessId(), AccessDTO.class);
			RoleDTO roleDTO = modelMapper.map(index.getRolId(), RoleDTO.class);
			accessRolesDTO.setAccessId(accessDTO);
			accessRolesDTO.setRolId(roleDTO);
			resp.add(accessRolesDTO);
		}
    	return resp;
    }
}
