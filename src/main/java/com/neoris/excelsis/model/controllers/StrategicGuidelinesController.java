package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.entities.InstitutionalObjects;
import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.StrategicGuidelines;
import com.neoris.excelsis.model.entities.StrategicGuidelines;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.services.InstitucionsServicio;
import com.neoris.excelsis.model.services.InstitutionalObjectsServicio;
import com.neoris.excelsis.model.services.MisionServicio;
import com.neoris.excelsis.model.services.StrategicGuidelinesServicio;
import com.neoris.excelsis.model.services.StrategicGuidelinesServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.output.LoginOutput;
import com.neoris.excelsis.security.GenericToken;

@RestController
@RequestMapping(value={"/excelsis-api/strategic_guidelines"})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.POST,RequestMethod.PUT})
public class StrategicGuidelinesController {

	@Autowired
	StrategicGuidelinesServicio strategicGuidelinesServicio;
	@Autowired
	InstitutionalObjectsServicio institutionalObjectsServicio; 
	
	@ResponseBody
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getstrategicGuidelinesById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        StrategicGuidelines strategicGuidelines = strategicGuidelinesServicio.findById(id);
        GenericOutput strategicGuidelinesOutput = new GenericOutput();
        if (strategicGuidelines == null) {
        	strategicGuidelinesOutput.setMessages("ERROR");
        	strategicGuidelinesOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }else {
        	strategicGuidelinesOutput.setObject(strategicGuidelines);
        	strategicGuidelinesOutput.setToken(token);
        	strategicGuidelinesOutput.setMessages("OK");
        	strategicGuidelinesOutput.setCodError("200");
        }
        return new ResponseEntity<GenericOutput>(strategicGuidelinesOutput, HttpStatus.OK);
    }

	@ResponseBody
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createstrategicGuidelines(@RequestBody StrategicGuidelines strategicGuidelines, @RequestHeader("Authorization") String token, UriComponentsBuilder ucBuilder){
		InstitutionalObjects institutionalObjects = institutionalObjectsServicio.findById(strategicGuidelines.getInstitutionObjectsId().getId());
		strategicGuidelines.setInstitutionObjectsId(institutionalObjects);
		strategicGuidelinesServicio.createStrategicGuidelines(strategicGuidelines);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/strategicGuidelines/{id}").buildAndExpand(strategicGuidelines.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

	@ResponseBody
    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllStrategicGuidelines(@RequestHeader("Authorization") String token) {
            ArrayList<StrategicGuidelines> tasks=(ArrayList<StrategicGuidelines>) strategicGuidelinesServicio.getAllStrategicGuidelines();
            GenericOutput genericOutput = new GenericOutput();
            if(!tasks.isEmpty()){
            	genericOutput.setObject(tasks);
            	genericOutput.setToken(token);
        		genericOutput.setMessages("OK");
        		genericOutput.setCodError("200");
            }else{
            	genericOutput.setMessages("ERROR");
            	genericOutput.setCodError("400");
            }
            return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updatestrategicGuidelines(@RequestBody StrategicGuidelines strategicGuidelines,@RequestHeader("Authorization") String token)
    {
        StrategicGuidelines strategicGuidelinesUpdate = strategicGuidelinesServicio.findById(strategicGuidelines.getId());
        if (strategicGuidelines==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        strategicGuidelinesUpdate=strategicGuidelines;
        InstitutionalObjects institutionalObjects = institutionalObjectsServicio.findById(strategicGuidelines.getInstitutionObjectsId().getId());
        strategicGuidelinesUpdate.setInstitutionObjectsId(institutionalObjects);
		strategicGuidelinesUpdate.setCreateDate(strategicGuidelinesUpdate.getCreateDate());
        strategicGuidelinesServicio.update(strategicGuidelinesUpdate, strategicGuidelinesUpdate.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericToken> deleteStrategicGuidelines(@PathVariable("id") long id){
    	GenericOutput genericOutput = new GenericOutput();
    	StrategicGuidelines strategicGuidelines = strategicGuidelinesServicio.findById(id);
        if (strategicGuidelines == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericToken>(HttpStatus.NOT_FOUND);
        }
        strategicGuidelinesServicio.deleteStrategicGuidelinesById(id);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericToken>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/update/{id}", headers="Accept=application/json")
    public ResponseEntity<StrategicGuidelines> updateStrategicGuidelinesPartially(@PathVariable("id") long id, @RequestBody StrategicGuidelines strategicGuidelines){
    	StrategicGuidelines strategicGuidelinesUpdate = strategicGuidelinesServicio.findById(id);
        if(strategicGuidelines ==null){
            return new ResponseEntity<StrategicGuidelines>(HttpStatus.NOT_FOUND);
        }
        strategicGuidelines.setCreateDate(strategicGuidelinesUpdate.getCreateDate());
        StrategicGuidelines usr = strategicGuidelinesServicio.updatePartially(strategicGuidelines, id);
        return new ResponseEntity<StrategicGuidelines>(strategicGuidelines, HttpStatus.OK);
    }
    
}
