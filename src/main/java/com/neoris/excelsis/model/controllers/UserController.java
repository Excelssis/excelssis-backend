package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.dto.UserDTO;
import com.neoris.excelsis.model.dto.UsersRolesDTO;
import com.neoris.excelsis.model.entities.Role;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.UsersRoles;
import com.neoris.excelsis.model.repositories.RoleRepositorio;
import com.neoris.excelsis.model.repositories.UserRepositorio;
import com.neoris.excelsis.model.services.RoleServicio;
import com.neoris.excelsis.model.services.RoleServicioImpl;
import com.neoris.excelsis.model.services.UserServicio;
import com.neoris.excelsis.model.services.UsersRolesServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.output.LoginOutput;
import com.neoris.excelsis.output.UserOutput;
import com.neoris.excelsis.security.GenericToken;

@RestController
@RequestMapping(value={"/excelsis-api/user"})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.POST,RequestMethod.PUT})
public class UserController {

	@Autowired
	UserServicio userServicio;
	@Autowired
	RoleServicio roleServicio;
	@Autowired
	UsersRolesServicio usersRolesServicio;
	
	@ResponseBody
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserOutput> getuserById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        User user = userServicio.findById(id);
        UserOutput userOutput = new UserOutput();
        if (user == null) {
        	userOutput.setMessages("ERROR");
        	userOutput.setCodError("400");
            return new ResponseEntity<UserOutput>(HttpStatus.NOT_FOUND);
        }else {
        	Collection<UsersRoles> roles;
        	roles = usersRolesServicio.findByUserId(user); 
        	List<UsersRoles> userRoles = new ArrayList<>(roles);
        	for (UsersRoles incex : userRoles) {
				incex.setUsersId(null);
			}
        	user.setUsersRoles(userRoles);
        	userOutput.setUser(user);
        	userOutput.setToken(token);
        	userOutput.setMessages("OK");
        	userOutput.setCodError("200");
        }
        return new ResponseEntity<UserOutput>(userOutput, HttpStatus.OK);
    }

	@ResponseBody
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createuser(@RequestBody User user, @RequestHeader("Authorization") String token, UriComponentsBuilder ucBuilder){
		userServicio.createUser(user);
        for (UsersRoles index : user.getUsersRoles()) {
        	Role rol = roleServicio.findById(index.getRolId().getId());
        	index.setRolId(rol);
        	index.setUsersId(user);
        	usersRolesServicio.createUsersRoles(index);
		}
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

	@ResponseBody
    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAlluser(@RequestHeader("Authorization") String token) {
            ArrayList<User> tasks=(ArrayList<User>) userServicio.getAllUser();
            ArrayList<UserDTO> usersDTO = new ArrayList<>();
            GenericOutput genericOutput = new GenericOutput();
            if(!tasks.isEmpty()){
            	for (User user : tasks) {
            		UserDTO userDTO = new UserDTO();   	
                	ModelMapper modelMapper = new ModelMapper();
                	userDTO = modelMapper.map(user, UserDTO.class);        
            		Collection<UsersRoles> usersRolesCollection;
            		usersRolesCollection = usersRolesServicio.findByUserId(user); 
                	List<UsersRoles> userRoles = new ArrayList<>(usersRolesCollection);
                	ArrayList<UsersRolesDTO> usersRolesDTO = new ArrayList<>();
                	for (UsersRoles index : userRoles) {
                		UsersRolesDTO userRolDTO = new UsersRolesDTO();
                		
                		ModelMapper modelMapperRolUser = new ModelMapper();
                		userRolDTO = modelMapper.map(index, UsersRolesDTO.class); 
                		
                		Role rol = roleServicio.findById(index.getRolId().getId());
                		RoleDTO roleDTO = new RoleDTO();
                		ModelMapper modelMapperRol = new ModelMapper();
                		roleDTO = modelMapper.map(rol, RoleDTO.class);  
                		
                		userRolDTO.setRolId(roleDTO);
                		
                		usersRolesDTO.add(userRolDTO);
        			}
                	userDTO.setUsersRoles(usersRolesDTO);
                	usersDTO.add(userDTO);
				}
            	genericOutput.setObject(usersDTO);
            	genericOutput.setToken(token);
        		genericOutput.setMessages("OK");
        		genericOutput.setCodError("200");
            }else{
            	genericOutput.setMessages("ERROR");
            	genericOutput.setCodError("400");
            }
            return genericOutput;
    }

    @PutMapping(value="/updateUser", headers="Accept=application/json")
    public ResponseEntity<String> updateuser(@RequestBody User user,@RequestHeader("Authorization") String token)
    {
        User userUpdate = userServicio.findById(user.getId());
        if (user==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        userUpdate = user;
        userUpdate.setCreateDate(user.getCreateDate());
        userServicio.update(user, user.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericToken> deleteUser(@PathVariable("id") long id){
    	GenericOutput genericOutput = new GenericOutput();
    	User user = userServicio.findById(id);
        if (user == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericToken>(HttpStatus.NOT_FOUND);
        }
        System.out.println("UserController.deleteUser()  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        Collection<UsersRoles> roles;
    	roles = usersRolesServicio.findByUserId(user); 
    	List<UsersRoles> userRoles = new ArrayList<>(roles);
    	
        for (UsersRoles index : userRoles) {
        	System.out.println("UserController.deleteUser()  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! "+index.getId());
        	usersRolesServicio.deleteUsersRolesById(index.getId());
        	
		}
        userServicio.deleteUserById(id);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericToken>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/updateUser/{id}", headers="Accept=application/json")
    public ResponseEntity<UserDTO> updateUserPartially(@PathVariable("id") long id, @RequestBody UserDTO userDTO){
    	User user = userServicio.findById(id);
        if(user ==null){
            return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
        }
        ModelMapper modelMapper = new ModelMapper();
        userDTO.setCreateDate(user.getCreateDate());
        User currentuser = modelMapper.map(userDTO, User.class);        
        User usr = userServicio.updatePartially(currentuser, id);
        return new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);
    }
    
    
    @ResponseBody
    @PostMapping(value="/login",headers="Accept=application/json")
    public LoginOutput findUserByLogin(@RequestParam("user") String username, @RequestParam("password") String pwd, UriComponentsBuilder ucBuilder){
    	List<User> users = userServicio.findByNickNamePass(username, pwd);
    	LoginOutput loginOutput = new LoginOutput();
    	GenericToken genericToken = new GenericToken();
    	String token = genericToken.getJWTToken(username);
    	if(!users.isEmpty()) {
    		User user = new User();
        	user = users.get(0);
        	Collection<UsersRoles> roles;
        	roles = usersRolesServicio.findByUserId(user); 
        	List<UsersRoles> userRoles = new ArrayList<>(roles);
        	for (UsersRoles incex : userRoles) {
				incex.setUsersId(null);
			}
        	user.setUsersRoles(userRoles);
    		loginOutput.setUser(users.get(0));
    		loginOutput.setToken(token);
    		loginOutput.setMessages("OK");
    		loginOutput.setCodError("200");
    	}else {
    		loginOutput.setMessages("ERROR");
    		loginOutput.setCodError("400");
    	}
        return loginOutput;
    }
}
