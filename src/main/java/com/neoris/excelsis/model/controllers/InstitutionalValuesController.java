package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.InstitutionalValues;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.services.InstitucionsServicio;
import com.neoris.excelsis.model.services.MisionServicio;
import com.neoris.excelsis.model.services.InstitutionalValuesServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.output.LoginOutput;
import com.neoris.excelsis.security.GenericToken;

@RestController
@RequestMapping(value={"/excelsis-api/institutionalValues"})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.POST,RequestMethod.PUT})
public class InstitutionalValuesController {

	@Autowired
	InstitutionalValuesServicio institutionalValuesServicio;
	@Autowired
	InstitucionsServicio institucionsServicio;
	
	@ResponseBody
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getinstitutionalValuesById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        InstitutionalValues institutionalValues = institutionalValuesServicio.findById(id);
        GenericOutput institutionalValuesOutput = new GenericOutput();
        if (institutionalValues == null) {
        	institutionalValuesOutput.setMessages("ERROR");
        	institutionalValuesOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }else {
        	institutionalValuesOutput.setObject(institutionalValues);
        	institutionalValuesOutput.setToken(token);
        	institutionalValuesOutput.setMessages("OK");
        	institutionalValuesOutput.setCodError("200");
        }
        return new ResponseEntity<GenericOutput>(institutionalValuesOutput, HttpStatus.OK);
    }

	@ResponseBody
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createinstitutionalValues(@RequestBody InstitutionalValues institutionalValues, @RequestHeader("Authorization") String token, UriComponentsBuilder ucBuilder){
		Institucions institucions=institucionsServicio.findById(institutionalValues.getInstitutionId().getId());
		institutionalValues.setInstitutionId(institucions);
		institutionalValuesServicio.createInstitutionalValues(institutionalValues);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/institutionalValues/{id}").buildAndExpand(institutionalValues.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

	@ResponseBody
    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllInstitutionalValues(@RequestHeader("Authorization") String token) {
            ArrayList<InstitutionalValues> tasks=(ArrayList<InstitutionalValues>) institutionalValuesServicio.getAllInstitutionalValues();
            GenericOutput genericOutput = new GenericOutput();
            if(!tasks.isEmpty()){
            	genericOutput.setObject(tasks);
            	genericOutput.setToken(token);
        		genericOutput.setMessages("OK");
        		genericOutput.setCodError("200");
            }else{
            	genericOutput.setMessages("ERROR");
            	genericOutput.setCodError("400");
            }
            return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updateinstitutionalValues(@RequestBody InstitutionalValues institutionalValues,@RequestHeader("Authorization") String token)
    {
        InstitutionalValues institutionalValuesUpdate = institutionalValuesServicio.findById(institutionalValues.getId());
        if (institutionalValues==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        institutionalValuesUpdate = institutionalValues;
        Institucions institucions=institucionsServicio.findById(institutionalValues.getInstitutionId().getId());
		institutionalValues.setInstitutionId(institucions);
		
		
		institutionalValuesUpdate.setCreateDate(institutionalValuesUpdate.getCreateDate());
        institutionalValuesServicio.update(institutionalValuesUpdate, institutionalValuesUpdate.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericToken> deleteInstitutionalValues(@PathVariable("id") long id){
    	GenericOutput genericOutput = new GenericOutput();
    	InstitutionalValues institutionalValues = institutionalValuesServicio.findById(id);
        if (institutionalValues == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericToken>(HttpStatus.NOT_FOUND);
        }
        institutionalValuesServicio.deleteInstitutionalValuesById(id);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericToken>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/update/{id}", headers="Accept=application/json")
    public ResponseEntity<InstitutionalValues> updateInstitutionalValuesPartially(@PathVariable("id") long id, @RequestBody InstitutionalValues institutionalValues){
    	InstitutionalValues institutionalValuesUpdate = institutionalValuesServicio.findById(id);
        if(institutionalValues ==null){
            return new ResponseEntity<InstitutionalValues>(HttpStatus.NOT_FOUND);
        }
        institutionalValues.setCreateDate(institutionalValuesUpdate.getCreateDate());
        InstitutionalValues usr = institutionalValuesServicio.updatePartially(institutionalValues, id);
        return new ResponseEntity<InstitutionalValues>(institutionalValues, HttpStatus.OK);
    }
    
}
