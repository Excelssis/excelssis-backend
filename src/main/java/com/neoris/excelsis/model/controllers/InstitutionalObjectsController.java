package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.InstitutionalObjects;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.services.InstitucionsServicio;
import com.neoris.excelsis.model.services.MisionServicio;
import com.neoris.excelsis.model.services.InstitutionalObjectsServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.output.LoginOutput;
import com.neoris.excelsis.security.GenericToken;

@RestController
@RequestMapping(value={"/excelsis-api/institutionalObjects"})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.POST,RequestMethod.PUT})
public class InstitutionalObjectsController {

	@Autowired
	InstitutionalObjectsServicio institutionalObjectsServicio;
	@Autowired
	InstitucionsServicio institucionsServicio;
	
	@ResponseBody
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getinstitutionalObjectsById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        InstitutionalObjects institutionalObjects = institutionalObjectsServicio.findById(id);
        GenericOutput institutionalObjectsOutput = new GenericOutput();
        if (institutionalObjects == null) {
        	institutionalObjectsOutput.setMessages("ERROR");
        	institutionalObjectsOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }else {
        	institutionalObjectsOutput.setObject(institutionalObjects);
        	institutionalObjectsOutput.setToken(token);
        	institutionalObjectsOutput.setMessages("OK");
        	institutionalObjectsOutput.setCodError("200");
        }
        return new ResponseEntity<GenericOutput>(institutionalObjectsOutput, HttpStatus.OK);
    }

	@ResponseBody
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createinstitutionalObjects(@RequestBody InstitutionalObjects institutionalObjects, @RequestHeader("Authorization") String token, UriComponentsBuilder ucBuilder){
		Institucions institucions=institucionsServicio.findById(institutionalObjects.getInstitutionId().getId());
		institutionalObjects.setInstitutionId(institucions);
		institutionalObjectsServicio.createInstitutionalObjects(institutionalObjects);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/institutionalObjects/{id}").buildAndExpand(institutionalObjects.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

	@ResponseBody
    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllInstitutionalObjects(@RequestHeader("Authorization") String token) {
            ArrayList<InstitutionalObjects> tasks=(ArrayList<InstitutionalObjects>) institutionalObjectsServicio.getAllInstitutionalObjects();
            GenericOutput genericOutput = new GenericOutput();
            if(!tasks.isEmpty()){
            	genericOutput.setObject(tasks);
            	genericOutput.setToken(token);
        		genericOutput.setMessages("OK");
        		genericOutput.setCodError("200");
            }else{
            	genericOutput.setMessages("ERROR");
            	genericOutput.setCodError("400");
            }
            return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updateinstitutionalObjects(@RequestBody InstitutionalObjects institutionalObjects,@RequestHeader("Authorization") String token)
    {
        InstitutionalObjects institutionalObjectsUpdate = institutionalObjectsServicio.findById(institutionalObjects.getId());
        if (institutionalObjects==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        institutionalObjectsUpdate=institutionalObjects;
        Institucions institucions=institucionsServicio.findById(institutionalObjects.getInstitutionId().getId());
        institutionalObjectsUpdate.setInstitutionId(institucions);
		institutionalObjectsUpdate.setCreateDate(institutionalObjectsUpdate.getCreateDate());
        institutionalObjectsServicio.update(institutionalObjectsUpdate, institutionalObjectsUpdate.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericToken> deleteInstitutionalObjects(@PathVariable("id") long id){
    	GenericOutput genericOutput = new GenericOutput();
    	InstitutionalObjects institutionalObjects = institutionalObjectsServicio.findById(id);
        if (institutionalObjects == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericToken>(HttpStatus.NOT_FOUND);
        }
        institutionalObjectsServicio.deleteInstitutionalObjectsById(id);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericToken>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/update/{id}", headers="Accept=application/json")
    public ResponseEntity<InstitutionalObjects> updateInstitutionalObjectsPartially(@PathVariable("id") long id, @RequestBody InstitutionalObjects institutionalObjects){
    	InstitutionalObjects institutionalObjectsUpdate = institutionalObjectsServicio.findById(id);
        if(institutionalObjects ==null){
            return new ResponseEntity<InstitutionalObjects>(HttpStatus.NOT_FOUND);
        }
        institutionalObjects.setCreateDate(institutionalObjectsUpdate.getCreateDate());
        InstitutionalObjects usr = institutionalObjectsServicio.updatePartially(institutionalObjects, id);
        return new ResponseEntity<InstitutionalObjects>(institutionalObjects, HttpStatus.OK);
    }
    
}
