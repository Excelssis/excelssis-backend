package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.Degrees;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.services.InstitucionsServicio;
import com.neoris.excelsis.model.services.MisionServicio;
import com.neoris.excelsis.model.services.DegreesServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.output.LoginOutput;
import com.neoris.excelsis.security.GenericToken;

@RestController
@RequestMapping(value={"/excelsis-api/degrees"})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.POST,RequestMethod.PUT})
public class DegreesController {

	@Autowired
	DegreesServicio degreesServicio;
	@Autowired
	InstitucionsServicio institucionsServicio;
	
	@ResponseBody
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getdegreesById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        Degrees degrees = degreesServicio.findById(id);
        GenericOutput degreesOutput = new GenericOutput();
        if (degrees == null) {
        	degreesOutput.setMessages("ERROR");
        	degreesOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }else {
        	degreesOutput.setObject(degrees);
        	degreesOutput.setToken(token);
        	degreesOutput.setMessages("OK");
        	degreesOutput.setCodError("200");
        }
        return new ResponseEntity<GenericOutput>(degreesOutput, HttpStatus.OK);
    }

	@ResponseBody
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createdegrees(@RequestBody Degrees degrees, @RequestHeader("Authorization") String token, UriComponentsBuilder ucBuilder){
		Institucions institucions=institucionsServicio.findById(degrees.getInstitutionId().getId());
		degrees.setInstitutionId(institucions);
		degreesServicio.createDegrees(degrees);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/degrees/{id}").buildAndExpand(degrees.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

	@ResponseBody
    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllDegrees(@RequestHeader("Authorization") String token) {
            ArrayList<Degrees> tasks=(ArrayList<Degrees>) degreesServicio.getAllDegrees();
            GenericOutput genericOutput = new GenericOutput();
            if(!tasks.isEmpty()){
            	genericOutput.setObject(tasks);
            	genericOutput.setToken(token);
        		genericOutput.setMessages("OK");
        		genericOutput.setCodError("200");
            }else{
            	genericOutput.setMessages("ERROR");
            	genericOutput.setCodError("400");
            }
            return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updatedegrees(@RequestBody Degrees degrees,@RequestHeader("Authorization") String token)
    {
        Degrees degreesUpdate = degreesServicio.findById(degrees.getId());
        if (degrees==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        degreesUpdate = degrees;
        Institucions institucions=institucionsServicio.findById(degrees.getInstitutionId().getId());
        degreesUpdate.setInstitutionId(institucions);
		degreesUpdate.setCreateDate(degreesUpdate.getCreateDate());
        degreesServicio.update(degreesUpdate, degreesUpdate.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericToken> deleteDegrees(@PathVariable("id") long id){
    	GenericOutput genericOutput = new GenericOutput();
    	Degrees degrees = degreesServicio.findById(id);
        if (degrees == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericToken>(HttpStatus.NOT_FOUND);
        }
        degreesServicio.deleteDegreesById(id);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericToken>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/update/{id}", headers="Accept=application/json")
    public ResponseEntity<Degrees> updateDegreesPartially(@PathVariable("id") long id, @RequestBody Degrees degrees){
    	Degrees degreesUpdate = degreesServicio.findById(id);
        if(degrees ==null){
            return new ResponseEntity<Degrees>(HttpStatus.NOT_FOUND);
        }
        degrees.setCreateDate(degreesUpdate.getCreateDate());
        Degrees usr = degreesServicio.updatePartially(degrees, id);
        return new ResponseEntity<Degrees>(degrees, HttpStatus.OK);
    }
    
}
