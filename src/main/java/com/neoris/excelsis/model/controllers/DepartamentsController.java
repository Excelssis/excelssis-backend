package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.AcademicPlans;
import com.neoris.excelsis.model.entities.AcademicPrograms;
import com.neoris.excelsis.model.entities.Departaments;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.services.InstitucionsServicio;
import com.neoris.excelsis.model.services.MisionServicio;
import com.neoris.excelsis.model.services.AcademicPlansServicio;
import com.neoris.excelsis.model.services.AcademicProgramsServicio;
import com.neoris.excelsis.model.services.DepartamentsServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.output.LoginOutput;
import com.neoris.excelsis.security.GenericToken;

@RestController
@RequestMapping(value={"/excelsis-api/departaments"})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.POST,RequestMethod.PUT})
public class DepartamentsController {

	@Autowired
	DepartamentsServicio departamentsServicio;
	@Autowired
	AcademicProgramsServicio academicProgramsServicio;
	
	@ResponseBody
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getdepartamentsById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        Departaments departaments = departamentsServicio.findById(id);
        GenericOutput departamentsOutput = new GenericOutput();
        if (departaments == null) {
        	departamentsOutput.setMessages("ERROR");
        	departamentsOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }else {
        	departamentsOutput.setObject(departaments);
        	departamentsOutput.setToken(token);
        	departamentsOutput.setMessages("OK");
        	departamentsOutput.setCodError("200");
        }
        return new ResponseEntity<GenericOutput>(departamentsOutput, HttpStatus.OK);
    }

	@ResponseBody
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createdepartaments(@RequestBody Departaments departaments, @RequestHeader("Authorization") String token, UriComponentsBuilder ucBuilder){
		AcademicPrograms academicPrograms=academicProgramsServicio.findById(departaments.getAcademicProgramsId().getId());
		departaments.setAcademicProgramsId(academicPrograms);
		departamentsServicio.createDepartaments(departaments);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/departaments/{id}").buildAndExpand(departaments.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

	@ResponseBody
    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllDepartaments(@RequestHeader("Authorization") String token) {
            ArrayList<Departaments> tasks=(ArrayList<Departaments>) departamentsServicio.getAllDepartaments();
            GenericOutput genericOutput = new GenericOutput();
            if(!tasks.isEmpty()){
            	genericOutput.setObject(tasks);
            	genericOutput.setToken(token);
        		genericOutput.setMessages("OK");
        		genericOutput.setCodError("200");
            }else{
            	genericOutput.setMessages("ERROR");
            	genericOutput.setCodError("400");
            }
            return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updatedepartaments(@RequestBody Departaments departaments,@RequestHeader("Authorization") String token)
    {
        Departaments departamentsUpdate = departamentsServicio.findById(departaments.getId());
        if (departaments==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        AcademicPrograms academicPrograms=academicProgramsServicio.findById(departaments.getAcademicProgramsId().getId());
        departamentsUpdate.setAcademicProgramsId(academicPrograms);
		departamentsUpdate.setCreateDate(departamentsUpdate.getCreateDate());
        departamentsServicio.update(departamentsUpdate, departamentsUpdate.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericToken> deleteDepartaments(@PathVariable("id") long id){
    	GenericOutput genericOutput = new GenericOutput();
    	Departaments departaments = departamentsServicio.findById(id);
        if (departaments == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericToken>(HttpStatus.NOT_FOUND);
        }
        departamentsServicio.deleteDepartamentsById(id);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericToken>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/update/{id}", headers="Accept=application/json")
    public ResponseEntity<Departaments> updateDepartamentsPartially(@PathVariable("id") long id, @RequestBody Departaments departaments){
    	Departaments departamentsUpdate = departamentsServicio.findById(id);
        if(departaments ==null){
            return new ResponseEntity<Departaments>(HttpStatus.NOT_FOUND);
        }
        departaments.setCreateDate(departamentsUpdate.getCreateDate());
        Departaments usr = departamentsServicio.updatePartially(departaments, id);
        return new ResponseEntity<Departaments>(departaments, HttpStatus.OK);
    }
    
}
