package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.neoris.excelsis.model.entities.Component;
import com.neoris.excelsis.model.services.RoleServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.model.services.ComponentServicio;


@RestController
@RequestMapping(value={"/excelsis-api/components"})
public class ComponentController {

	@Autowired
	ComponentServicio componentServicio;
	@Autowired
	RoleServicio roleServicio;
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getcomponentsById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
       Component components = componentServicio.findById(id);
       GenericOutput genericOutput = new GenericOutput();
        if (components == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }
        genericOutput.setObject(components);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericOutput>(genericOutput, HttpStatus.OK);
    }

    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createComponents(@RequestHeader("Authorization") String token, @RequestBody Component component, UriComponentsBuilder ucBuilder){
    	Component components = modelMapper.map(component, Component.class);
        componentServicio.createComponent(components);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/components/{id}").buildAndExpand(components.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllcomponents(@RequestHeader("Authorization") String token) {
    	GenericOutput genericOutput = new GenericOutput();
        List<Component> components=componentServicio.getComponent();
        if(!components.isEmpty()) {
        	genericOutput.setObject(components);
            genericOutput.setMessages("OK");
            genericOutput.setCodError("200");	
        }else {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");	
        }
        return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updatecomponents(@RequestHeader("Authorization") String token, @RequestBody Component componentUpdate)
    {
        Component components = componentServicio.findById(componentUpdate.getId());
        if (components==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        componentUpdate.setCreateDate(components.getCreateDate());
        componentUpdate.setModify(new Date());
        componentServicio.update(componentUpdate, componentUpdate.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<Component> deleteComponents(@RequestHeader("Authorization") String token, @PathVariable("id") long id){
    	Component components = componentServicio.findById(id);
        if (components == null) {
            return new ResponseEntity<Component>(HttpStatus.NOT_FOUND);
        }
        componentServicio.deleteComponentById(id);
        return new ResponseEntity<Component>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/updateComponent/{id}", headers="Accept=application/json")
    public ResponseEntity<Component> updateComponentsPartially(@RequestHeader("Authorization") String token, @PathVariable("id") long id, @RequestBody Component component){
    	Component components = componentServicio.findById(id);
        if(components ==null){
            return new ResponseEntity<Component>(HttpStatus.NOT_FOUND);
        }
        component.setCreateDate(components.getCreateDate());
        components = modelMapper.map(component, Component.class);
        componentServicio.updatePartially(components, id);
        return new ResponseEntity<Component>(component, HttpStatus.OK);
    }
}
