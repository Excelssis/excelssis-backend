package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Role;
import com.neoris.excelsis.model.entities.UserCountryInstitution;
import com.neoris.excelsis.model.repositories.RoleRepositorio;
import com.neoris.excelsis.model.repositories.UserCountryInstitutionRepositorio;
import com.neoris.excelsis.model.services.RoleServicio;
import com.neoris.excelsis.model.services.RoleServicioImpl;
import com.neoris.excelsis.model.services.UserCountryInstitutionServicio;

@RestController
@RequestMapping(value={"/userCountryInstitution"})
public class UserCountryInstitutionController {

    @Autowired
    UserCountryInstitutionServicio userCountryInstitutionServicio;
    

    
    @GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserCountryInstitution> getuserCountryInstitutionById(@PathVariable("id") long id) {
        UserCountryInstitution userCountryInstitution = userCountryInstitutionServicio.findById(id);
        if (userCountryInstitution == null) {
            return new ResponseEntity<UserCountryInstitution>(HttpStatus.NOT_FOUND);
        }   
        return new ResponseEntity<UserCountryInstitution>(userCountryInstitution, HttpStatus.OK);
    }

    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createuserCountryInstitution(@RequestBody UserCountryInstitution userCountryInstitution, UriComponentsBuilder ucBuilder){ 
        userCountryInstitutionServicio.createUserCountryInstitution(userCountryInstitution);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/userCountryInstitution/{id}").buildAndExpand(userCountryInstitution.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<UserCountryInstitution> getAlluserCountryInstitution() {
            ArrayList<UserCountryInstitution> tasks=(ArrayList<UserCountryInstitution>) userCountryInstitutionServicio.getUserCountryInstitution();
            return tasks;
    }

    @PutMapping(value="/updateUserCountryInstitution", headers="Accept=application/json")
    public ResponseEntity<String> updateuserCountryInstitution(@RequestBody UserCountryInstitution userCountryInstitutionDTO)
    {
        UserCountryInstitution userCountryInstitution = userCountryInstitutionServicio.findById(userCountryInstitutionDTO.getId());
        if (userCountryInstitution==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        userCountryInstitutionDTO.setCreate(userCountryInstitution.getCreate());
        ModelMapper modelMapper = new ModelMapper();
        userCountryInstitution = modelMapper.map(userCountryInstitutionDTO, UserCountryInstitution.class);
        userCountryInstitutionServicio.update(userCountryInstitution, userCountryInstitution.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<UserCountryInstitution> deleteUserCountryInstitution(@PathVariable("id") long id){
        UserCountryInstitution userCountryInstitution = userCountryInstitutionServicio.findById(id);
        userCountryInstitutionServicio.deleteUserCountryInstitutionById(id);
        return new ResponseEntity<UserCountryInstitution>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/updateUserCountryInstitution/{id}", headers="Accept=application/json")
    public ResponseEntity<UserCountryInstitution> updateUserCountryInstitutionPartially(@PathVariable("id") long id, @RequestBody UserCountryInstitution userCountryInstitution){
        UserCountryInstitution userCountryInstitutionResp = userCountryInstitutionServicio.findById(id);
        if(userCountryInstitution ==null){
            return new ResponseEntity<UserCountryInstitution>(HttpStatus.NOT_FOUND);
        }
        UserCountryInstitution usr = userCountryInstitutionServicio.updatePartially(userCountryInstitutionResp, id);
        return new ResponseEntity<UserCountryInstitution>(userCountryInstitutionResp, HttpStatus.OK);
    }
   
    @GetMapping(value = "/findUser/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<UserCountryInstitution> getuserCountryInstitutionByIdUser(@PathVariable("id") long id) {
    	ArrayList<UserCountryInstitution> userCountryInstitution = (ArrayList<UserCountryInstitution>) userCountryInstitutionServicio.findByIdUser(id);
        return userCountryInstitution;
    }
}
