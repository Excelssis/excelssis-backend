package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.Campus;
import com.neoris.excelsis.model.entities.Installations;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.services.InstitucionsServicio;
import com.neoris.excelsis.model.services.MisionServicio;
import com.neoris.excelsis.model.services.CampusServicio;
import com.neoris.excelsis.model.services.InstallationsServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.output.LoginOutput;
import com.neoris.excelsis.security.GenericToken;

@RestController
@RequestMapping(value={"/excelsis-api/installations"})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.POST,RequestMethod.PUT})
public class InstallationsController {

	@Autowired
	InstallationsServicio installationsServicio;
	@Autowired
	CampusServicio campusServicio;
	
	@ResponseBody
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getinstallationsById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        Installations installations = installationsServicio.findById(id);
        GenericOutput installationsOutput = new GenericOutput();
        if (installations == null) {
        	installationsOutput.setMessages("ERROR");
        	installationsOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }else {
        	installationsOutput.setObject(installations);
        	installationsOutput.setToken(token);
        	installationsOutput.setMessages("OK");
        	installationsOutput.setCodError("200");
        }
        return new ResponseEntity<GenericOutput>(installationsOutput, HttpStatus.OK);
    }

	@ResponseBody
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createinstallations(@RequestBody Installations installations, @RequestHeader("Authorization") String token, UriComponentsBuilder ucBuilder){
		Campus campusId=campusServicio.findById(installations.getCampusId().getId());
		installations.setCampusId(campusId);
		installationsServicio.createInstallations(installations);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/installations/{id}").buildAndExpand(installations.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

	@ResponseBody
    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllInstallations(@RequestHeader("Authorization") String token) {
            ArrayList<Installations> tasks=(ArrayList<Installations>) installationsServicio.getAllInstallations();
            GenericOutput genericOutput = new GenericOutput();
            if(!tasks.isEmpty()){
            	genericOutput.setObject(tasks);
            	genericOutput.setToken(token);
        		genericOutput.setMessages("OK");
        		genericOutput.setCodError("200");
            }else{
            	genericOutput.setMessages("ERROR");
            	genericOutput.setCodError("400");
            }
            return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updateinstallations(@RequestBody Installations installations,@RequestHeader("Authorization") String token)
    {
        Installations installationsUpdate = installationsServicio.findById(installations.getId());
        if (installations==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        Campus campusId=campusServicio.findById(installations.getCampusId().getId());
		installations.setCampusId(campusId);
		installationsUpdate = installations;
		installationsUpdate.setCreateDate(installationsUpdate.getCreateDate());
        installationsServicio.update(installationsUpdate, installationsUpdate.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericToken> deleteInstallations(@PathVariable("id") long id){
    	GenericOutput genericOutput = new GenericOutput();
    	Installations installations = installationsServicio.findById(id);
        if (installations == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericToken>(HttpStatus.NOT_FOUND);
        }
        installationsServicio.deleteInstallationsById(id);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericToken>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/update/{id}", headers="Accept=application/json")
    public ResponseEntity<Installations> updateInstallationsPartially(@PathVariable("id") long id, @RequestBody Installations installations){
    	Installations installationsUpdate = installationsServicio.findById(id);
        if(installations ==null){
            return new ResponseEntity<Installations>(HttpStatus.NOT_FOUND);
        }
        installations.setCreateDate(installationsUpdate.getCreateDate());
        Installations usr = installationsServicio.updatePartially(installations, id);
        return new ResponseEntity<Installations>(installations, HttpStatus.OK);
    }
    
}
