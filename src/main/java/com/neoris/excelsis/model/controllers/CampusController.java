package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.Campus;
import com.neoris.excelsis.model.entities.Degrees;
import com.neoris.excelsis.model.entities.Installations;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.services.InstitucionsServicio;
import com.neoris.excelsis.model.services.MisionServicio;
import com.neoris.excelsis.model.services.CampusServicio;
import com.neoris.excelsis.model.services.DegreesServicio;
import com.neoris.excelsis.model.services.InstallationsServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.output.LoginOutput;
import com.neoris.excelsis.security.GenericToken;

@RestController
@RequestMapping(value={"/excelsis-api/campus"})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.POST,RequestMethod.PUT})
public class CampusController {

	@Autowired
	CampusServicio campusServicio;
	@Autowired
	InstitucionsServicio institucionsServicio;
	@Autowired
	DegreesServicio degreesServicio;
	@Autowired
	InstallationsServicio installationsServicios;
	
	@ResponseBody
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getcampusById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        Campus campus = campusServicio.findById(id);
        GenericOutput campusOutput = new GenericOutput();
        if (campus == null) {
        	campusOutput.setMessages("ERROR");
        	campusOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }else {
        	campusOutput.setObject(campus);
        	campusOutput.setToken(token);
        	campusOutput.setMessages("OK");
        	campusOutput.setCodError("200");
        }
        return new ResponseEntity<GenericOutput>(campusOutput, HttpStatus.OK);
    }

	@ResponseBody
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createcampus(@RequestBody Campus campus, @RequestHeader("Authorization") String token, UriComponentsBuilder ucBuilder){
		Institucions institucions=institucionsServicio.findById(campus.getInstitutionId().getId());
		Installations installationId = installationsServicios.findById(campus.getInstallationId().getId());
		campus.setInstitutionId(institucions);
		campus.setInstallationId(installationId);
		campusServicio.createCampus(campus);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/campus/{id}").buildAndExpand(campus.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

	@ResponseBody
    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllCampus(@RequestHeader("Authorization") String token) {
            ArrayList<Campus> tasks=(ArrayList<Campus>) campusServicio.getAllCampus();
            GenericOutput genericOutput = new GenericOutput();
            if(!tasks.isEmpty()){
            	genericOutput.setObject(tasks);
            	genericOutput.setToken(token);
        		genericOutput.setMessages("OK");
        		genericOutput.setCodError("200");
            }else{
            	genericOutput.setMessages("ERROR");
            	genericOutput.setCodError("400");
            }
            return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updatecampus(@RequestBody Campus campus,@RequestHeader("Authorization") String token)
    {
        Campus campusUpdate = campusServicio.findById(campus.getId());
        if (campus==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        campusUpdate = campus;
        Institucions institucions=institucionsServicio.findById(campus.getInstitutionId().getId());
		Installations installationId = installationsServicios.findById(campus.getInstallationId().getId());
		campusUpdate.setInstitutionId(institucions);
		campusUpdate.setInstallationId(installationId);	
		campusUpdate.setCreateDate(campusUpdate.getCreateDate());
        campusServicio.update(campusUpdate, campusUpdate.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericToken> deleteCampus(@PathVariable("id") long id){
    	GenericOutput genericOutput = new GenericOutput();
    	Campus campus = campusServicio.findById(id);
        if (campus == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericToken>(HttpStatus.NOT_FOUND);
        }
        campusServicio.deleteCampusById(id);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericToken>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/update/{id}", headers="Accept=application/json")
    public ResponseEntity<Campus> updateCampusPartially(@PathVariable("id") long id, @RequestBody Campus campus){
    	Campus campusUpdate = campusServicio.findById(id);
        if(campus ==null){
            return new ResponseEntity<Campus>(HttpStatus.NOT_FOUND);
        }
        campus.setCreateDate(campusUpdate.getCreateDate());
        Campus usr = campusServicio.updatePartially(campus, id);
        return new ResponseEntity<Campus>(campus, HttpStatus.OK);
    }
    
}
