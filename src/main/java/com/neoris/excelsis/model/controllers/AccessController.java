package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.AccessComponentsDTO;
import com.neoris.excelsis.model.dto.AccessDTO;
import com.neoris.excelsis.model.dto.ComponentDTO;
import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Access;
import com.neoris.excelsis.model.entities.AccessComponents;
import com.neoris.excelsis.model.entities.AccessRoles;
import com.neoris.excelsis.model.entities.Component;
import com.neoris.excelsis.model.entities.Role;
import com.neoris.excelsis.model.entities.Access;
import com.neoris.excelsis.model.repositories.RoleRepositorio;
import com.neoris.excelsis.model.repositories.AccessRepositorio;
import com.neoris.excelsis.model.services.AccessComponentsServicio;
import com.neoris.excelsis.model.services.AccessRolesServicio;
import com.neoris.excelsis.model.services.AccessServicio;
import com.neoris.excelsis.model.services.ComponentServicio;
import com.neoris.excelsis.model.services.RoleServicio;
import com.neoris.excelsis.model.services.RoleServicioImpl;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.model.services.AccessServicio;

@RestController
@RequestMapping(value={"/excelsis-api/access"})
public class AccessController {

	@Autowired
	AccessServicio accessServicio;
	@Autowired
	ComponentServicio componentServicio;
	@Autowired
	AccessComponentsServicio accessComponentsServicio;

	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getaccessById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        Access access = accessServicio.findById(id);
        GenericOutput genericOutput = new GenericOutput();
        if (access == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }
        genericOutput.setObject(access);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericOutput>(genericOutput, HttpStatus.OK);
    }

    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createAccess(@RequestHeader("Authorization") String token, @RequestBody Access access, UriComponentsBuilder ucBuilder){
        accessServicio.createAccess(access);
        
        for (AccessComponents index : access.getAccessComponent()) {
      	  Component component = modelMapper.map(index.getComponentsId(), Component.class);
      	  AccessComponents accessComponents = new AccessComponents();
      	  accessComponents.setAccessId(access);
      	  accessComponents.setComponentsId(component);
      	  accessComponents.setFunction(index.getFunction());
      	  accessComponentsServicio.createAccessComponents(accessComponents);
        }        
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllAccess(@RequestHeader("Authorization") String token) {
            ArrayList<Access> tasks=(ArrayList<Access>) accessServicio.getAccess();
            ArrayList<AccessDTO> accesses = new ArrayList<AccessDTO>();
            GenericOutput genericOutput = new GenericOutput();
            if(!tasks.isEmpty()) {
            	for (Access access : tasks) {
                	AccessDTO accessDTO = new AccessDTO();
                    accessDTO.setId(access.getId());
                    accessDTO.setName(access.getName());
                    accessDTO.setDescription(access.getDescription());
                    accessDTO.setCreateDate(access.getCreateDate());
                    accessDTO.setModify(access.getModify());
                    accessDTO.setStatus(access.isStatus());
                    accessDTO.setCommentary(access.getCommentary());
                    accessDTO.setAccessComponent(getAccessComponentsDTOS(access.getAccessComponent()));
                    accesses.add(accessDTO);
    			}
            	genericOutput.setObject(accesses);
	            genericOutput.setMessages("OK");
	            genericOutput.setCodError("200");	
	        }else {
	        	genericOutput.setMessages("ERROR");
	            genericOutput.setCodError("400");	
	        }
            return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updateAccess(@RequestHeader("Authorization") String token, @RequestBody AccessDTO accessDTO)
    {
    	System.out.println("AccessController.updateAccess()");
    	Access access = accessServicio.findById(accessDTO.getId());
        if(access ==null){
        	return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        accessDTO.setCreateDate(access.getCreateDate());
        
        access.setName(accessDTO.getName());
    	access.setDescription(accessDTO.getDescription());
    	access.setCommentary(accessDTO.getCommentary());
    	access.setModify(accessDTO.getModify());
    	access.setStatus(accessDTO.isStatus());
    	
    	ArrayList<AccessComponents> accessComponentsList = new ArrayList<AccessComponents>();
        
        for (AccessComponentsDTO index : accessDTO.getAccessComponent()) {
      	  Component component = modelMapper.map(index.getComponentsId(), Component.class);
      	  AccessComponents accessComponents = new AccessComponents();
      	  accessComponents.setAccessId(access);
      	  accessComponents.setComponentsId(component);
      	  accessComponents.setFunction(index.getFunction());
      	  accessComponents.setModify(index.getModify());
      	  accessComponents.setId(index.getId());
      	  accessComponents.setCreateDate(access.getCreateDate());
      	  accessComponents.setCommentary(index.getCommentary());
      	  accessComponentsServicio.updatePartially(accessComponents, accessComponents.getId());
      	  accessComponentsList.add(accessComponents);
        }
        access.setAccessComponent(accessComponentsList);
        accessServicio.updatePartially(access, access.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<AccessDTO> deleteAccess(@RequestHeader("Authorization") String token, @PathVariable("id") long id){
    	Access access = accessServicio.findById(id);
        if (access == null) {
            return new ResponseEntity<AccessDTO>(HttpStatus.NOT_FOUND);
        }
        AccessDTO accessDTO = modelMapper.map(access, AccessDTO.class);
        accessServicio.deleteAccessById(id);
        return new ResponseEntity<AccessDTO>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/updateComponent/{id}", headers="Accept=application/json")
    public ResponseEntity<AccessDTO> updateAccessPartially(@RequestHeader("Authorization") String token, @PathVariable("id") long id, @RequestBody AccessDTO accessDTO){
    	Access access = accessServicio.findById(id);
        if(access ==null){
            return new ResponseEntity<AccessDTO>(HttpStatus.NOT_FOUND);
        }
        accessDTO.setCreateDate(access.getCreateDate());
        
        access.setName(accessDTO.getName());
    	access.setDescription(accessDTO.getDescription());
    	access.setCommentary(accessDTO.getCommentary());
    	access.setModify(accessDTO.getModify());
    	access.setStatus(accessDTO.isStatus());
    	
    	ArrayList<AccessComponents> accessComponentsList = new ArrayList<AccessComponents>();
        
        for (AccessComponentsDTO index : accessDTO.getAccessComponent()) {
      	  Component component = modelMapper.map(index.getComponentsId(), Component.class);
      	  AccessComponents accessComponents = new AccessComponents();
      	  accessComponents.setAccessId(access);
      	  accessComponents.setComponentsId(component);
      	  accessComponents.setFunction(index.getFunction());
      	  accessComponentsServicio.updatePartially(accessComponents, accessComponents.getId());
      	  accessComponentsList.add(accessComponents);
        }
        access.setAccessComponent(accessComponentsList);
        accessServicio.updatePartially(access, id);
        return new ResponseEntity<AccessDTO>(accessDTO, HttpStatus.OK);
    }
    @GetMapping(value="/getAllAccessComponents", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<AccessDTO> getAllAccessComponents(@RequestHeader("Authorization") String token) {
            ArrayList<Access> tasks=(ArrayList<Access>) accessServicio.getAccess();
            ArrayList<AccessDTO> accessDTOs = new ArrayList<AccessDTO>();
            for (Access index : tasks) {
            	AccessDTO accessDTO = modelMapper.map(index, AccessDTO.class);
            	accessDTOs.add(accessDTO);
    		}
            return accessDTOs;
    }
    
    
   private ArrayList<AccessComponentsDTO> getAccessComponentsDTOS(List<AccessComponents> accessComponents){
	   ArrayList<AccessComponentsDTO> accessComponentsDTOs = new ArrayList<AccessComponentsDTO>();
	   for (AccessComponents index : accessComponents) {
		   AccessComponentsDTO accessComponentsDTO = modelMapper.map(index,AccessComponentsDTO.class);
		   accessComponentsDTOs.add(accessComponentsDTO);
	}
	   
	   return accessComponentsDTOs;
   }
}
