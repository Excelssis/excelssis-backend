package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.neoris.excelsis.model.dto.ComponentDTO;
import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.entities.Component;
import com.neoris.excelsis.model.entities.Role;
import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.repositories.RoleRepositorio;
import com.neoris.excelsis.model.repositories.InstitucionsRepositorio;
import com.neoris.excelsis.model.services.InstitucionsServicio;
import com.neoris.excelsis.model.services.ComponentServicio;
import com.neoris.excelsis.model.services.RoleServicio;
import com.neoris.excelsis.model.services.RoleServicioImpl;
import com.neoris.excelsis.model.services.UserServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.model.services.InstitucionsServicio;

@RestController
@RequestMapping(value={"/excelsis-api/institucions"})
public class InstitucionsController {

    @Autowired
    InstitucionsServicio institucionsServicio;
    @Autowired
    UserServicio userServicio;
    

    ModelMapper modelMapper = new ModelMapper();
    
    @GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getinstitucionsById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        Institucions institucions = institucionsServicio.findById(id);
        GenericOutput genericOutput = new GenericOutput();
        if (institucions == null) {
        	genericOutput.setMessages("ERROR");
        	genericOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }
        genericOutput.setObject(institucions);
        genericOutput.setMessages("OK");
		genericOutput.setCodError("200");
        return new ResponseEntity<GenericOutput>(genericOutput, HttpStatus.OK);
    }

    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createInstitucions(@RequestHeader("Authorization") String token, @RequestBody Institucions institucions, UriComponentsBuilder ucBuilder){
        institucionsServicio.createInstitucions(institucions);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllInstitucions(@RequestHeader("Authorization") String token) {
    	GenericOutput genericOutput = new GenericOutput();
        ArrayList<Institucions> tasks=(ArrayList<Institucions>) institucionsServicio.getInstitucions();
        if(!tasks.isEmpty()){
        	genericOutput.setObject(tasks);
        	genericOutput.setToken(token);
    		genericOutput.setMessages("OK");
    		genericOutput.setCodError("200");
        }else{
        	genericOutput.setMessages("ERROR");
        	genericOutput.setCodError("400");
        }
        return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updateInstitucions(@RequestHeader("Authorization") String token, @RequestBody Institucions institucions)
    {
        Institucions institucionsResp = institucionsServicio.findById(institucions.getId());
        if(institucions ==null){
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        institucionsResp.setCreateDate(institucions.getCreateDate());
        institucionsServicio.updatePartially(institucions, institucions.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericOutput> deleteInstitucions(@RequestHeader("Authorization") String token, @PathVariable("id") long id){
        Institucions institucions = institucionsServicio.findById(id);
        GenericOutput genericOutput = new GenericOutput();
        if (institucions == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }
        institucionsServicio.deleteInstitucionsById(id);
        return new ResponseEntity<GenericOutput>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/update/{id}", headers="Accept=application/json")
    public ResponseEntity<Institucions> updateInstitucionsPartially(@RequestHeader("Authorization") String token, @PathVariable("id") long id, @RequestBody Institucions institucions){
        Institucions institucionsResp = institucionsServicio.findById(id);
        if(institucionsResp ==null){
            return new ResponseEntity<Institucions>(HttpStatus.NOT_FOUND);
        }
        institucionsServicio.updatePartially(institucions, id);
        return new ResponseEntity<Institucions>(institucionsResp, HttpStatus.OK);
    }
    @GetMapping(value="/getAllInstitucions", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Institucions> getAllInstitucionsComponents() {
            ArrayList<Institucions> tasks=(ArrayList<Institucions>) institucionsServicio.getInstitucions();
            return tasks;
    }
}
