package com.neoris.excelsis.model.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.lang.reflect.Type;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.neoris.excelsis.model.dto.RoleDTO;
import com.neoris.excelsis.model.entities.Institucions;
import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.repositories.MisionRepositorio;
import com.neoris.excelsis.model.services.InstitucionsServicio;
import com.neoris.excelsis.model.services.MisionServicio;
import com.neoris.excelsis.output.GenericOutput;
import com.neoris.excelsis.output.LoginOutput;
import com.neoris.excelsis.security.GenericToken;

@RestController
@RequestMapping(value={"/excelsis-api/mision"})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE,RequestMethod.POST,RequestMethod.PUT})
public class MisionController {

	@Autowired
	MisionServicio misionServicio;
	@Autowired
	InstitucionsServicio institucionsServicio;
	
	@ResponseBody
	@GetMapping(value = "/find/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericOutput> getmisionById(@RequestHeader("Authorization") String token, @PathVariable("id") long id) {
        Mision mision = misionServicio.findById(id);
        GenericOutput misionOutput = new GenericOutput();
        if (mision == null) {
        	misionOutput.setMessages("ERROR");
        	misionOutput.setCodError("400");
            return new ResponseEntity<GenericOutput>(HttpStatus.NOT_FOUND);
        }else {
        	misionOutput.setObject(mision);
        	misionOutput.setToken(token);
        	misionOutput.setMessages("OK");
        	misionOutput.setCodError("200");
        }
        return new ResponseEntity<GenericOutput>(misionOutput, HttpStatus.OK);
    }

	@ResponseBody
    @PostMapping(value="/create",headers="Accept=application/json")
    public ResponseEntity<Void> createmision(@RequestBody Mision mision, @RequestHeader("Authorization") String token, UriComponentsBuilder ucBuilder){
		Institucions institucions=institucionsServicio.findById(mision.getInstitutionId().getId());
		mision.setInstitutionId(institucions);
		misionServicio.createMision(mision);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/mision/{id}").buildAndExpand(mision.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

	@ResponseBody
    @GetMapping(value="/get", headers="Accept=application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericOutput getAllmision(@RequestHeader("Authorization") String token) {
            ArrayList<Mision> tasks=(ArrayList<Mision>) misionServicio.getAllMision();
            GenericOutput genericOutput = new GenericOutput();
            if(!tasks.isEmpty()){
            	genericOutput.setObject(tasks);
            	genericOutput.setToken(token);
        		genericOutput.setMessages("OK");
        		genericOutput.setCodError("200");
            }else{
            	genericOutput.setMessages("ERROR");
            	genericOutput.setCodError("400");
            }
            return genericOutput;
    }

    @PutMapping(value="/update", headers="Accept=application/json")
    public ResponseEntity<String> updatemision(@RequestBody Mision mision,@RequestHeader("Authorization") String token)
    {
        Mision misionUpdate = misionServicio.findById(mision.getId());
        if (mision==null) {
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }
        
        Institucions institucions=institucionsServicio.findById(mision.getInstitutionId().getId());
		mision.setInstitutionId(institucions);
		
        misionUpdate = mision;
        misionUpdate.setCreateDate(misionUpdate.getCreateDate());
        misionServicio.update(misionUpdate, misionUpdate.getId());
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}", headers ="Accept=application/json")
    public ResponseEntity<GenericToken> deleteMision(@PathVariable("id") long id){
    	GenericOutput genericOutput = new GenericOutput();
    	Mision mision = misionServicio.findById(id);
        if (mision == null) {
        	genericOutput.setMessages("ERROR");
            genericOutput.setCodError("400");
            return new ResponseEntity<GenericToken>(HttpStatus.NOT_FOUND);
        }
        misionServicio.deleteMisionById(id);
        genericOutput.setMessages("OK");
        genericOutput.setCodError("200");
        return new ResponseEntity<GenericToken>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/update/{id}", headers="Accept=application/json")
    public ResponseEntity<Mision> updateMisionPartially(@PathVariable("id") long id, @RequestBody Mision mision){
    	Mision misionUpdate = misionServicio.findById(id);
        if(mision ==null){
            return new ResponseEntity<Mision>(HttpStatus.NOT_FOUND);
        }
        mision.setCreateDate(misionUpdate.getCreateDate());
        Mision usr = misionServicio.updatePartially(mision, id);
        return new ResponseEntity<Mision>(mision, HttpStatus.OK);
    }
    
}
