package com.neoris.excelsis.model.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.User;

public interface UserRepositorio extends CrudRepository<User, Long>, JpaRepository<User, Long>{

	@Query("SELECT u FROM User u WHERE u.nickName= ?1 AND u.password= ?2")
	List<User> findByNicknamePass(String nickName, String pass);
	
//	@Query("SELECT u FROM dev2.users u JOIN dev2.users_roles ur ON u.id=ur.id_user JOIN dev2.roles r ON r.id=ur.id_rol")
//	List<User> getAllUser();
	
//	SELECT DISTINCT e FROM Employee e INNER JOIN e.tasks t where t.supervisor = e.name
	
//	
//	
//	 @Query("SELECT use from Useres fetch join use.profiles where user.id = use.idusuario = (?1)")
//	 List<Users> userdata(int id);
}
