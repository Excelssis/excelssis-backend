package com.neoris.excelsis.model.repositories;

import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.UserCountryInstitution;

public interface UserCountryInstitutionRepositorio extends CrudRepository<UserCountryInstitution, Long>{

}
