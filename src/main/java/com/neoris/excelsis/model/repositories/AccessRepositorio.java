package com.neoris.excelsis.model.repositories;

import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.Access;

public interface AccessRepositorio extends CrudRepository<Access, Long>{

}
