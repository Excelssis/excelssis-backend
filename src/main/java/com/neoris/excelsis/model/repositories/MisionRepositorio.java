package com.neoris.excelsis.model.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;

public interface MisionRepositorio extends CrudRepository<Mision, Long>, JpaRepository<Mision, Long>{
}
