package com.neoris.excelsis.model.repositories;

import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.Component;

public interface ComponentRepositorio extends CrudRepository<Component, Long>{

}
