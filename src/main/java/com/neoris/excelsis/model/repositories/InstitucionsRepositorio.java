package com.neoris.excelsis.model.repositories;

import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.Institucions;

public interface InstitucionsRepositorio extends CrudRepository<Institucions, Long>{

}
