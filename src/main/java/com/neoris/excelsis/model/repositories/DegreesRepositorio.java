package com.neoris.excelsis.model.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.Degrees;
import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.Vision;

public interface DegreesRepositorio extends CrudRepository<Degrees, Long>, JpaRepository<Degrees, Long>{

}
