package com.neoris.excelsis.model.repositories;

import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.Countries;
import com.neoris.excelsis.model.entities.Role;

public interface CountriesRepositorio extends CrudRepository<Countries, Long>{

}
