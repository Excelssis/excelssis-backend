package com.neoris.excelsis.model.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.User;
import com.neoris.excelsis.model.entities.UsersRoles;

public interface UsersRolesRepositorio extends CrudRepository<UsersRoles, Long>, JpaRepository<UsersRoles, Long>{

	@Query("SELECT u FROM UsersRoles u WHERE u.userId= ?1")
	Collection<UsersRoles> findByUserId(User userId);
}
