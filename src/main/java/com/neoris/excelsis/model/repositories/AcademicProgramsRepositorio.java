package com.neoris.excelsis.model.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.AcademicPrograms;

public interface AcademicProgramsRepositorio extends CrudRepository<AcademicPrograms, Long>, JpaRepository<AcademicPrograms, Long>{
}
