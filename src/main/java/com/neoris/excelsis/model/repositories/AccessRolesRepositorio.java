package com.neoris.excelsis.model.repositories;

import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.AccessRoles;

public interface AccessRolesRepositorio extends CrudRepository<AccessRoles, Long>{

}
