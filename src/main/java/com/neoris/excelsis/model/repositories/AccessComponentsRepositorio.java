package com.neoris.excelsis.model.repositories;

import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.AccessComponents;

public interface AccessComponentsRepositorio extends CrudRepository<AccessComponents, Long>{

}
