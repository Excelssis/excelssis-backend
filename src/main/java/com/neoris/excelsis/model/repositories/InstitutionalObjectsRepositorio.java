package com.neoris.excelsis.model.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.InstitutionalObjects;

public interface InstitutionalObjectsRepositorio extends CrudRepository<InstitutionalObjects, Long>, JpaRepository<InstitutionalObjects, Long>{
}
