package com.neoris.excelsis.model.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.neoris.excelsis.model.entities.InstitutionalValues;
import com.neoris.excelsis.model.entities.Mision;
import com.neoris.excelsis.model.entities.User;

public interface InstitutionalValuesRepositorio extends CrudRepository<InstitutionalValues, Long>, JpaRepository<InstitutionalValues, Long>{
}
