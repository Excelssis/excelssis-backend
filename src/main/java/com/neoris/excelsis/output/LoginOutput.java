package com.neoris.excelsis.output;

import java.io.Serializable;

import com.neoris.excelsis.model.entities.User;

public class LoginOutput implements Serializable{

	private User user;
	private String token;
	private String messages;
	private String codError;
	
	public LoginOutput() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public LoginOutput(User user, String token, String messages, String codError) {
		super();
		this.user = user;
		this.token = token;
		this.messages = messages;
		this.codError = codError;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getMessages() {
		return messages;
	}
	public void setMessages(String messages) {
		this.messages = messages;
	}
	public String getCodError() {
		return codError;
	}
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	
	
}
