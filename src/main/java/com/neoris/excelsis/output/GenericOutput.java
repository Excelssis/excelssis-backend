package com.neoris.excelsis.output;

import java.io.Serializable;

import com.neoris.excelsis.model.entities.User;

public class GenericOutput implements Serializable{

	private Object object;
	private String token;
	private String messages;
	private String codError;
	
	public GenericOutput() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public GenericOutput( Object object, String token, String messages, String codError) {
		super();
		this.object = object;
		this.token = token;
		this.messages = messages;
		this.codError = codError;
	}
	
	
	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getMessages() {
		return messages;
	}
	public void setMessages(String messages) {
		this.messages = messages;
	}
	public String getCodError() {
		return codError;
	}
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	
	
}
